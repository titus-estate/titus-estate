############################################
# Setup Server
############################################
set :stage, :production
set :stage_url, "http://titus-karlen1993.rhcloud.com"
server "titus-karlen1993.rhcloud.com", user: "572f5fd889f5cf4a100000e7", roles: %w{web app db}
set :deploy_to, "/var/lib/openshift/572f5fd889f5cf4a100000e7/app-root/data"

############################################
# Setup Git
############################################

set :branch, "master"

############################################
# Extra Settings
############################################

#specify extra ssh options:

#set :ssh_options, {
#    auth_methods: %w(password),
#    password: 'password',
#    user: 'username',
#}

#specify a specific temp dir if user is jailed to home
#set :tmp_dir, "/path/to/custom/tmp"
