############################################
# Slack Configuration
############################################

# Required
set :slack_subdomain, 'youcorp' # if your Slack subdomain is example.slack.com
set :slack_url, 'https://hooks.slack.com/services/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' # https://my.slack.com/services/new/incoming-webhook

# Optional
set :slack_channel, '#titusestate'
set :slack_emoji, ':rocket:'
