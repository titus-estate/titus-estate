var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var theme_path = './content/themes/titus';
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require("clean-webpack-plugin");
var WebpackOnBuildPlugin = require("on-build-webpack");
var WebpackGenerateIncludesPlugin = require("./webpack/generate-includes-plugin");
require('es6-promise').polyfill();
const ENV = (process.argv[2] ==  '--env' && process.argv[3] == 'production') ? 'production' :  'development';
module.exports = [
    {
        name: "js",
        entry: {
            "app": theme_path + "/assets/js/main.js",
        },
        output: {
            path: theme_path+"/assets/build",
            filename: "[name].min.js",
        },

        module: {
            loaders:[{
                test: /\.jsx?$/,
                loader: 'babel',
                query: {
                    presets: ["react", "es2015"]
                }
            }]
        },

        resolve: {
            root: path.resolve('./'),
            alias:{
                modules: path.resolve( './content/themes/titus/modules' ),
                utils: path.resolve( './content/themes/titus/assets/js/utils' ),
            },
        },

        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify(ENV)
                }
            }),

            new WebpackGenerateIncludesPlugin(),

            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery",
                Materialize: "content/themes/titus/assets/js/vendor/materialize.js",
                noUiSlider: "content/themes/titus/assets/js/vendor/nouislider.js",
                stickyScroll: "content/themes/titus/assets/js/vendor/sticky-scroll.js",
                PubSub: "pubsub-js",
                hammerjs: "hammerjs",
                Hummer: "hammerjs",
                React: "react",
                ReactDOM: "react-dom"
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    drop_console: false
                }
            }),
        ]
    },
    {
        name: "css",
        entry: {
            "app": theme_path + "/assets/scss/main.scss",
        },
        output: {
            path: theme_path+"/assets/build",
            filename: "style.min.js"
        },

        module: {
            loaders:[{
                test: /\.scss?$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")
            }]
        },

        resolve: {
            root: path.resolve('./')
        },

        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify(ENV)
                }
            }),

            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    drop_console: false,
                },
                output: {
                    comments: false
                }
            }),
            new ExtractTextPlugin("app.min.css", {
                allChunks: true
            }),
            new CleanWebpackPlugin(['style.min.js', "app.min.js", "app.min.css"], {
                root: path.resolve('./')+"/content/themes/titus/assets/build",
                verbose: true,
                dry: false
            }),
            new WebpackOnBuildPlugin(function(stats) {
                var file = path.resolve('./')+"/content/themes/titus/assets/build/style.min.js";
                fs.unlink(file, function(){
                    console.log(file+" has been removed.")
                });

            })
        ]
    }
];

