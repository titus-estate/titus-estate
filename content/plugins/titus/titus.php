<?php
/**
 * Entry point for the plugin.
 *
 * This file is read by WordPress to generate the plugin information in the
 * admin panel.
 *
 * @package WP_Plugin
 *
 * Plugin Name: Titus
 * Author: Karlen Avetisyan <karlen.avetisyan@astgeek.com>
 * Description: Titus theme main functionality.
 * Version:     0.0.1
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}
require_once "autoload.php";
add_action( 'admin_init', 'titus_init' );

function titus() {

}

add_action( 'admin_menu', 'titus_plugin_menu' );

function titus_plugin_menu() {
	$icon = plugin_dir_url( __FILE__ ).'/assets/img/logo_small.png';
	add_menu_page( 'Titus', 'Titus', 'manage_options', 'titus', 'titus_admin_render', $icon, 38 );

}

function titus_admin_render() {

}

function load_module($moduleName,  $args = false){
	$module = '\Modules\\'.$moduleName;
	$module = new $module;
	$args ? $module->init($args) : $module->init();

}

function get_module($moduleName,  $args = false){
	$module = '\Modules\\'.$moduleName;
	$module = new $module;
	return $args ? $module->init_get($args) : $module->init_get();
}


function get_post_slider_images( $post_id, $size = 'full' ) {

	$featured_image_src = '';
	$featured_image_id = get_post_thumbnail_id( $post_id );
	$slider_images = array();

	if ( !empty( $featured_image_id ) ) {
		$featured_image = wp_get_attachment_image_src( $featured_image_id, $size );
		if ( ! empty( $featured_image ) ) {
			$slider_images[] = $featured_image[0];
		}
	}

	$banners = get_field( 'slider_images', $post_id );
	if( is_array($size) ) {
		if( !empty($size) ) {
			$size = 'thumb-' . $size[0] . 'x' . $size[1];
		}
	}


	if(is_array($banners)) {
        foreach ($banners as $banner) {

            if ( ! empty( $banner ) && isset( $banner['sizes'][ $size ] ) ) {
                $slider_images[] = $banner['sizes'][ $size ];
            } elseif (!empty($banner['url'])) {
                $slider_images[]  = $banner['url'];
            }

        }
    }


    return $slider_images;
}


function get_post_image_src( $post_id, $size = 'full' ) {

	$featured_image_src = '';
	$featured_image_id = get_post_thumbnail_id( $post_id );

	if ( empty( $featured_image_id ) ) {
		$banners = get_field( 'slider_images', $post_id );

		if( is_array($size) ) {
			if( !empty($size) ) {
				$size = 'thumb-' . $size[0] . 'x' . $size[1];
			}
		}

		if ( ! empty( $banners[0] ) && isset( $banners[0]['sizes'][ $size ] ) ) {
			$featured_image_src = $banners[0]['sizes'][ $size ];
		} elseif (!empty($banners[0]['url'])) {
			$featured_image_src = $banners[0]['url'];
		}
	} else {
		$featured_image = wp_get_attachment_image_src( $featured_image_id, $size );

		if ( ! empty( $featured_image ) ) {
			$featured_image_src = $featured_image[0];
		}
	}

	return $featured_image_src;
}


function tags_to_string($ID) {
    $tags = wp_get_object_terms( $ID, 'post_tag');
    $ret = "";
    foreach ($tags as $tag){
        $ret .= ucfirst($tag->name).", ";
    }
    $ret = trim($ret, ', ');
    return $ret;
}