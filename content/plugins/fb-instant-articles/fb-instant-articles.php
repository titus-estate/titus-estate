<?php
/**
 * Entry point for the plugin.
 *
 * This file is read by WordPress to generate the plugin information in the
 * admin panel.
 *
 * @package WP_Plugin
 *
 * Plugin Name: Facebook instant articles
 * Author: Karlen Avetisyan <karlen19931016@gmail.com>
 * Description: Export posts to facebook instant articles.
 * Version:     0.0.1
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'admin_init', 'fb_instant_articles_init' );

function fb_instant_articles_init() {
	require plugin_dir_path( __FILE__ ) . 'autoload.php';
	(new FbInstantArticles\Metabox())->addAjaxActions();
}

/**
 * Plugin page
 */
add_action( 'admin_menu', 'fb_instant_articles' );

add_action( 'add_meta_boxes', 'fb_instant_articles_meta_box_render');

function fb_instant_articles() {
	add_menu_page( 'Instant Articles', 'Instant Articles', 'manage_options', 'fb_instant_articles', 'fb_instant_articles_render', 'dashicons-facebook-alt', 38 );
	add_submenu_page ('fb_instant_articles', 'Settings', 'Settings', 'manage_options', 'fb_instant_articles_settings', 'fb_instant_articles_settings');
}

function fb_instant_articles_render() {

	require plugin_dir_path( __FILE__ ) . '/vendor/autoload.php';
	$admin = new FbInstantArticles\Admin();
	$admin->render();
}
function fb_instant_articles_settings() {
	require plugin_dir_path( __FILE__ ) . '/vendor/autoload.php';
	$settings = new FbInstantArticles\Settings();
	$settings->render();
}


function fb_instant_articles_meta_box_render(){
	$metabox = new FbInstantArticles\Metabox();
	$metabox->showMetaBox();
}