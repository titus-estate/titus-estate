<?php
/**
 * Autoload cp publish to facebook instant articles classes.
 */
spl_autoload_register( function ( $class ) {
    $isClass = explode('\\', $class);
    if($isClass[0] == "FbInstantArticles"){

        $path = strtolower( $class );
        $path = str_replace( '_', '-',  $path );
        $path = explode( '\\', $path );

        $file = array_pop( $path );
        $path = implode( '/', $path ) . '/' . $file . '.class.php';

        $path = realpath( __DIR__ . '/' . $path );

        if ( file_exists( $path ) ) {
            require_once $path;
        }
    }
} );
