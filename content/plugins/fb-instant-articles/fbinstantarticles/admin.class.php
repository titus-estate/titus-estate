<?php
namespace FbInstantArticles;
/**
 * Class Admin
 * @package FbInstantArticles
 */
class Admin {

    protected

        $post_types  = array('post'),
        $posts_per_page = 20,
        $total_pages = 0,
        $paged = 1,
        $total_count = 0,
        $errors = array();

    public function __construct() {

        $this->showMessages();
        $this->do_action();
        $this->paged = (int) $_REQUEST['paged'];
        $this->total_count = $this->getTotalCount();
    }


    private function showMessages(){

        $errors = array();


        $token = get_option('facebook_instant_articles_aceess_token');
        if(!$token || empty($token)){
            $errors[] = 'invalid';
        }
        $html = "";
        if(!empty($errors)) {
            $settingsUrl = menu_page_url('fb_instant_articles_settings', false);
            foreach ($errors as $error) {
                $error_txt = "<p>Invalid access token</p><p><a href='".$settingsUrl."'>Please update access tokens</a></p>";
                $html .=  '<div class="error notice">' . $error_txt . '</div>';
            }
        }

        echo $html;
    }

    protected function getTotalCount(){

        if(isset($_REQUEST['s']) && !empty($_REQUEST['s'])){
            return 0;
        }
        global $wpdb;
        $status_query = $_REQUEST['instant_articles_status'];

        if( !empty($status_query) ) {
            if ($status_query != "Not published") {
                $join = "INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
                WHERE ( ( wp_postmeta.meta_key = 'instant_articles_publish_status' AND CAST(wp_postmeta.meta_value AS CHAR) = '$status_query' ) )";
            }else{
                $join = "INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
                WHERE wp_postmeta.meta_key = 'instant_articles_publish_status' ) AND ( wp_postmeta.post_id IS NULL )";
            }
        } else {
            $join = "WHERE 1=1";
        }

        $sql = "SELECT COUNT(*) FROM wp_posts
                {$join}
                AND wp_posts.post_status = 'publish'  AND wp_posts.post_type = 'post' GROUP BY wp_posts.ID ";
        return  $wpdb->query($sql);
    }


    protected function getPosts() {

        $arr = array();

        $args = array(
            'post_type'         =>  $this->post_types,
            'post_status'       => 'publish',
            'suppress_filters'  => false,
            'posts_per_page'    => $this->posts_per_page,
            'paged' => $this->paged,
        );

        $status_query = $_REQUEST['instant_articles_status'];
        if(!empty($status_query)){
            if($status_query == "Not published"){
                $args['meta_query'] = array(
                    array(
                        'key' => 'instant_articles_publish_status',
                        'compare' => 'NOT EXISTS'
                    )
                );
            }else{
                $args['meta_query'] = array(
                    array(
                        'key' => 'instant_articles_publish_status',
                        'value' => $status_query,
                    )
                );
            }
        }

        if(isset($_REQUEST['s']) && !empty($_REQUEST['s'])){
            $args["s"] = sanitize_text_field($_REQUEST['s']);
            $args["paged"] = 1;
        }

        $posts = get_posts($args);
        foreach($posts as $post) {

            $instant_articles_update = get_post_meta($post->ID, 'instant_articles_update_time');
            $instant_articles_update = str_replace("T", " ", $instant_articles_update);
            $instant_articles_update = str_replace("Z", " ", $instant_articles_update);
            $instant_articles_status = get_post_meta($post->ID, 'instant_articles_publish_status');

            $instant_articles_update = empty($instant_articles_update) ? " " : $instant_articles_update[0];
            $instant_articles_status = empty($instant_articles_status) ? "Not published" : $instant_articles_status[0];

            $arr[] = array(
                "ID" => $post->ID,
                "title" => $post->post_title,
                "last_update" => $post->post_modified,
                "instant_articles_update" => $instant_articles_update,
                "status" => $instant_articles_status,
            );
        }
        return $arr;

    }


    protected function show_errors(){
        if(empty($this->errors)){
            return;
        }
        foreach($this->errors as $error){
            $error_txt = "<p>Article-".$error["ID"]." </p> <p>Message- ".$error["text"]."</p>";
            echo '<div class="error notice">'.$error_txt.'</div>';
        }
    }

    protected function do_action() {

        $action = $_REQUEST["action"];
        $posts =$_REQUEST["instant_articles_post"];

        if(empty($posts)){
            return;
        }

        switch($action){
            case "download":
                $this->download($posts);
                break;
            case "publish_as_preview":
                $this->publish($posts, true);
                break;
            case "publish":
                $this->publish($posts);
                break;
            case "update":
                $this->update($posts);
                break;
            case "delete":
                $this->delete($posts);
                break;
            default:
                return;
                break;

        }

    }

    protected function publish($posts, $is_preview = false) {
        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->publish($posts, $is_preview);
        wp_safe_redirect( wp_get_referer() );
        $this->show_errors();
    }

    protected function update($posts) {
        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->update($posts);
        wp_safe_redirect( wp_get_referer() );
        $this->show_errors();
    }


    protected function delete($posts) {
        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->delete($posts);
        wp_safe_redirect( wp_get_referer() );
        $this->show_errors();
    }


    protected function download($posts){
        $action = new \FbInstantArticles\Actions();
        $action->download($posts);
    }


    public  function render() {
        $posts = $this->getPosts();
        $listTable = new \FbInstantArticles\List_Table($posts,$this->total_count, $this->posts_per_page, $this->post_types);
        $listTable->prepare_items();
        ?>
        <div class="wrap">


            <h2>Facebook Instant Articles</h2>

            <form id="movies-filter" method="get">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <div style="clear: both; padding: 5px;"></div>
                <?php
                $listTable->search_box("Search", "instant_articles_search");
                ?>
                <div style="clear: both; padding: 5px;"></div>
                <?php
                $listTable->display();
                ?>
            </form>
        </div>
        <?php
    }

}