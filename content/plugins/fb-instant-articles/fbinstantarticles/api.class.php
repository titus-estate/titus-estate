<?php
namespace FbInstantArticles;
/**
 * Class API
 * @package FbInstantArticles
 */
class API {


    /**
     * Pages list per region
     * @var array
     */
    private $channels = array();

    const EDGE_NAME = '/instant_articles';



    private $page_id;
    private $app_id;
    private $app_secret;
    private $token;
    private $facebook;

    function __construct() {
        $settings =  new \FbInstantArticles\Settings();
        $this->page_id = $settings->page_id;
        $this->app_secret = $settings->app_secret;
        $this->app_id = $settings->app_id;
        $this->token = $settings->token;
        $this->facebook = new \Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_access_token' => $this->token,
            'default_graph_version' => 'v2.5'
        ]);
    }

    public function delete($post, $is_preview){
        $permalink = get_permalink($post);
        if ($articleID = $this->getArticleIDFromCanonicalURL($permalink)) {
            $this->facebook->delete($articleID);
            return true;
        }
        return true;
    }

    public function getArticleIDFromCanonicalURL($canonicalURL)
    {
        $response = $this->facebook->get('?id=' . $canonicalURL . '&fields=instant_article');
        $instantArticle = $response->getGraphNode()->getField('instant_article');
        if (!$instantArticle) {
            return null;
        }

        $articleID = $instantArticle->getField('id');
        return $articleID;
    }

    public function publish($article, $is_preview) {
        $article  = $article->to_instant_article();
        $this->facebook->post($this->page_id . self::EDGE_NAME, [
            'html_source' => $article->render(),
            'take_live' => false,
            'development_mode' => false,
        ]);
        return true;
    }

    public function getLastSubmissionStatus($articleID)
    {
        if (!$articleID) {
            return false;
        }

        $response = $this->facebook->get($articleID . '?fields=most_recent_import_status');
        $articleStatus = $response->getGraphNode()->getField('most_recent_import_status');

        $messages = array();
        if (isset($articleStatus['errors'])) {
            foreach ($articleStatus['errors'] as $error) {
                $messages[] = array(
                    "level" => $error['level'],
                    "message" => $error['message']
                );
            }
        }

        return array("status" => $articleStatus['status'], "messages" => $messages);
    }


}
