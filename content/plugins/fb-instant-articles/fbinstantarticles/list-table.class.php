<?php
namespace FbInstantArticles;
/**
 * Class List_Table
 * @package FbInstantArticles
 */
class List_Table extends \WP_List_Table {

    public $per_page,
        $total_items,
        $posts_data = array(),
        $post_types,
        $status = '0',
        $s = '',
        $type = "0";

    public function __construct($data, $total, $per_page, $post_types){

        parent::__construct( array(
            'singular'  => 'instant_articles_post',     //singular name of the listed records
            'plural'    => 'instant_articles_posts',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

        $this->posts_data = $data;
        $this->total_items = $total;
        $this->per_page = $per_page;
        $this->post_types = $post_types;


        $status_filter = $_REQUEST['instant_articles_status'];
        if(isset($status_filter) && !empty($status_filter) ){
            $this->status = $status_filter;
        }


        if(isset($_REQUEST['s']) && !empty($_REQUEST['s'])){
            $this->s = "&s=".$_REQUEST['s'];
        }
    }

    protected function column_default($item, $column_name){
        switch($column_name){
            case 'last_update':
            case 'instant_articles_update':
            case 'status':
                return $item[$column_name];
            default:
                return ""; //Show the whole array for troubleshooting purposes
        }
    }

    protected function column_title($item){

        //Build row actions
        $actions = array(
            'view'    => sprintf('<a href="%s" target="_blank">View post</a>', get_permalink($item['ID'])),
            'download'      => sprintf('<a href="/wordpress/wp-admin/admin-ajax.php?action=instant_articles_download&channel=0&id=%s">Download</a>', $item['ID']),
        );


        if($item["status"] == "Not published"){

            $actions["publish"] = sprintf('<a href="?page=%s&action=%s&instant_articles_post[]=%s&instant_articles_status=%s%s">Publish</a>',
                $_REQUEST['page'],'publish',$item['ID'], $this->status,  $this->s);


        }else if($item["status"] == "Published"){

            $actions['update']      = sprintf('<a href="?page=%s&action=%s&instant_articles_post[]=%s&instant_articles_status=%s%s">Update</a>',
                $_REQUEST['page'],'update',$item['ID'], $this->status,  $this->s);

            $actions['delete']    = sprintf('<a href="?page=%s&action=%s&instant_articles_post[]=%s&instant_articles_status=%s%s">Delete</a>',
                $_REQUEST['page'],'delete',$item['ID'], $this->status,  $this->s);

        }else if($item["status"] == "Preview"){

            $actions['delete']    = sprintf('<a href="?page=%s&action=%s&instant_articles_post[]=%s&instant_articles_status=%s%s">Delete</a>',
                $_REQUEST['page'],'delete',$item['ID'], $this->status,  $this->s);

            $actions["publish"] = sprintf('<a href="?page=%s&action=%s&instant_articles_post[]=%s&instant_articles_status=%s%s">Publish</a>',
                $_REQUEST['page'],'publish',$item['ID'], $this->status, $this->s);

        }

        return sprintf('%1$s <p style="color:silver">(id:%2$s)</p>%3$s',
            /*$1%s*/ $item['title'],
            /*$2%s*/ $item['ID'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }

    protected function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }

    public function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'title'     => 'Title',
            'last_update'    => 'Post last update time',
            'instant_articles_update'    => 'Instant article last update time',
            'status'  => 'Article Status',
        );
        return $columns;
    }

    protected function get_bulk_actions() {
        $actions = array(
            'publish'    => 'Publish',
            'update'    => 'Update',
            'delete'    => 'Delete',
        );
        return $actions;
    }



    protected function get_status_filter() {
        return ( empty( $_GET['instant_articles_status'] ) ) ? '' : sanitize_text_field( $_GET['instant_articles_status'] );
    }

    protected function status_filter_field(){
        // Add available statuses
        $statuses =  array(
            '0' => "All statuses - instant article status",
            'Published' => "Published",
            'Not published' => "Not published",
        );

        ?>
        <select name="instant_articles_status" id="instant_articles_status">
            <?php
            foreach ( $statuses as $value => $label ) {
                printf(
                    '<option value="%s" %s>%s</option>',
                    esc_attr( $value ),
                    selected( $value, $this->get_status_filter(), false ),
                    esc_html( $label )
                );
            }
            ?>
        </select>
        <?php
    }



    protected function extra_tablenav( $which ) {
        if ( 'top' != $which ) {
            return;
        }
        ?>
        <div class="alignleft actions">
            <?php
            // Add a publish state filter
            $this->status_filter_field();
            submit_button(  'Filter', 'button', 'filter_action', false, array( 'id' => 'post-query-submit' ) );
            ?>
        </div>
        <?php
    }

    public function prepare_items() {

        $columns = $this->get_columns();
        $hidden = array();
        $this->_column_headers = array($columns, $hidden);
        $data = $this->posts_data;
        $this->items = $data;
        $this->set_pagination_args( array(
            'total_items' => $this->total_items,                  //WE have to calculate the total number of items
            'per_page'    => $this->per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil( $this->total_items/ $this->per_page)   //WE have to calculate the total number of pages
        ) );
    }


}

