<?php
namespace FbInstantArticles;
/**
 * Class Content
 * @package FbInstantArticles
 */
class Content {

    private $content;

    public function __construct($content){
        $this->content = $content;
    }

    private function filterContent(){
        $content = $this->content;
        if( empty( $content )) {
            return '';
        }
        libxml_use_internal_errors( true );
        $dom = new \DOMDocument();
        $dom->encoding = 'utf8';
        $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
        $content = html_entity_decode($dom->saveHTML());
        $this->content = $content;
    }

    private function isSocialEmbed($url){
        $ret = false;
        $supportedProviders = array(
            "instagram.com",
            "instagr.am",
            "twitter.com",
            "vine.co",
            "youtube.com",
            "youtu.be",
        );

        foreach($supportedProviders as $provider){
            if(strpos($url, $provider) >= 1){
                $ret = true;
            }
        }

        return $ret;
    }

    private function hasIframeParent($node){
        if($node->parentNode->nodeName == 'iframe'){
            return true;
        }else if( $node->parentNode instanceOf \DOMElement){
            return $this->hasIframeParent($node->parentNode);
        }else{
            return false;
        }
    }

    private function parseBlockQuoteEmbeds(){
        $content = $this->content;
        libxml_use_internal_errors( true );
        $dom = new \DOMDocument();
        $dom->encoding = 'utf8';
        $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
        $embeds = $dom->getElementsByTagName('blockquote');
        $scripts = $dom->getElementsByTagName('script');
        foreach($scripts as $script){
            if($script->hasAttribute('src') && !$this->hasIframeParent($script)){
                $src = " ".$script->getAttribute('src');
                if(strpos($src, 'instagram') > 0 || (strpos($src, 'twitter') > 0)){
                    $script->parentNode->removeChild($script);
                }
            }
        }

        foreach($embeds as $embed){
            if(!$this->hasIframeParent($embed)){
                $newEl = $dom->createElement('figure');
                $newEl->setAttribute('class', 'op-social');
                $newFrame = $dom->createElement('iframe');
                $newEl->appendChild($newFrame);
                $newEmbed = $embed->cloneNode(true);
                $newFrame->appendChild($newEmbed);
                if($embed->hasAttribute('class')){
                    $className = " ".$embed->getAttribute('class');
                    if(strpos($className, 'instagram') > 0){
                        $scriptElem = $dom->createElement('script');
                        $scriptElem->setAttribute('src', '//platform.instagram.com/en_US/embeds.js');
                        $scriptElem->setAttribute('defer', 'defer');
                        $newFrame->appendChild($scriptElem);
                    }elseif(strpos($className, 'twitter') > 0){
                        $scriptElem = $dom->createElement('script');
                        $scriptElem->setAttribute('src', '//platform.twitter.com/widgets.js');
                        $scriptElem->setAttribute('charset', 'utf-8');
                        $newFrame->appendChild($scriptElem);
                    }
                }

                $embed->parentNode->replaceChild($newEl, $embed);
            }
        }
        $body = $dom->getElementsByTagName('body')->item(0);
        $html =  $dom->saveHTML($body);
        $html = str_replace('<body>', '', $html);
        $html = str_replace('</body>', '', $html);
        $this->content = $html;
    }

    private function parseIframeEmbeds(){
        $this->setInteractiveEmbeds();
        $content = $this->content;
        libxml_use_internal_errors( true );
        $dom = new \DOMDocument();
        $dom->encoding = 'utf8';
        $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
        $embeds = $dom->getElementsByTagName('iframe');
        foreach($embeds as $embed){
            $isInteractive = false;
            $parentNode = $embed->parentNode;
            if($parentNode->nodeName == 'figure' && $parentNode->hasAttribute('class') && $parentNode->getAttribute('class') == 'op-interactive')
            {
                $isInteractive = true;
            }
            if(!$isInteractive){
                $newEl = $dom->createElement('figure');
                $newEl->setAttribute('class', 'op-social');
                $newEmbed = $embed->cloneNode(true);
                $newEl->appendChild($newEmbed);
                $embed->parentNode->replaceChild($newEl, $embed);
            }

        }
        $body = $dom->getElementsByTagName('body')->item(0);
        $html =  $dom->saveHTML($body);
        $html = str_replace('<body>', '', $html);
        $html = str_replace('</body>', '', $html);
        $this->content = $html;

    }

    private function parseUrlEmbeds(){
        $html = $this->content;
        $re = "/[^\"'](https?:\/\/(?:w{1,3}.)?[^\s]*?(?:\.[a-z\/?&=_0-9\-]+)+)(?![^<]*?(?:<\/\w+>|\/?>))[^\"']/i";
        $html = preg_replace_callback(
            $re,
            function ($matches) {
                if(!empty($matches)){
                    $url = $matches[0];
                    $url = trim($url);
                    $embed = wp_oembed_get($url);
                    $oembed = _wp_oembed_get_object();
                    $provider = $oembed->get_provider($url);
                    if($this->isSocialEmbed($provider)){
                        if(strpos($provider, 'youtube.com') > 1 || strpos($provider, 'youtu.be') > 1){
                            if($embed){
                                return '<figure class="op-social">'.$embed."</figure>";
                            }else{
                                return $url;
                            }
                        }else{
                            if($embed){
                                return '<figure class="op-social"><iframe>'.$embed."</iframe></figure>";
                            }else{
                                return $url;
                            }
                        }
                    }else{
                        if($embed){
                            return '<figure class="op-interactive">'.$embed."</figure>";
                        }else{
                            return $url;
                        }
                    }
                }
            },
            $html
        );
        $this->content =  $html;
    }

    private function embed() {
        $this->parseIframeEmbeds();
        $this->parseBlockQuoteEmbeds();
        $this->parseUrlEmbeds();

    }

    private function setInteractiveEmbeds(){
        $content = $this->content;
        if( empty( $content )) {
            return '';
        }
        libxml_use_internal_errors( true );
        $dom = new \DOMDocument();
        $dom->encoding = 'utf8';
        $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
        $embeds = $dom->getElementsByTagName('iframe');

        $interactiveNodes = array();

        foreach ($embeds as $embed) {
            if($embed->hasAttribute('src')){
                $src = $embed->getAttribute('src');
                if(!$this->isSocialEmbed($src)){
                    $interactiveNodes[] = $embed;
                }
            }
        }

        foreach($interactiveNodes as $interactiveNode){
            $interactive = $dom->createElement('figure');
            $interactive->setAttribute('class', 'op-interactive');
            $interactiveClone = $interactiveNode->cloneNode(true);
            $interactive->appendChild($interactiveClone);
            $interactiveNode->parentNode->replaceChild($interactive, $interactiveNode);
        }
        $body = $dom->getElementsByTagName('body')->item(0);
        $html =  $dom->saveHTML($body);
        $html = str_replace('<body>', '', $html);
        $html = str_replace('</body>', '', $html);
        $this->content = $html;
    }

    private function closeTags($content){
        libxml_use_internal_errors( true );
        $dom = new \DOMDocument();
        $dom->encoding = 'utf8';
        $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
        $body = $dom->getElementsByTagName('body')->item(0);
        $html =  $dom->saveHTML($body);
        $html = str_replace('<body>', '', $html);
        $html = str_replace('</body>', '', $html);
        return $html;
    }

    private function set_content(){
        $content = $this->content;
        $content = wpautop($content);
        $ret = "";
        $content = str_replace("<figure>", '[layout-content]<figure>', $content);
        $content = str_replace('<figure class="op-social">', '[layout-content]<figure class="op-social">', $content);
        $content = str_replace("</figure>", '</figure>[layout-content]', $content);
        $html = explode('[layout-content]', $content);
        foreach($html as $layout){
            $layout = $this->closeTags($layout);
            $ret .= $layout;
        }
        $this->content = $ret;
    }

    private function parse() {
        $this->filterContent();
        $this->embed();
        $this->parseImageNodes();
        $this->set_content();
    }

    private function parseImageNodes(){
        $content = $this->content;
        $content = strip_shortcodes($content);
        libxml_use_internal_errors( true );
        $dom = new \DOMDocument();
        $dom->encoding = 'utf8';
        $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
        $imgs = $dom->getElementsByTagName('img');
        foreach($imgs as $img){
            $src = $img->getAttribute('src');
            $newEl = $dom->createElement('figure');
            $newImage = $dom->createElement('img');
            $newImage->setAttribute('src', $src);
            $newEl->appendChild($newImage);
            $img->parentNode->replaceChild($newEl, $img);
        }
        $body = $dom->getElementsByTagName('body')->item(0);
        $html =  $dom->saveHTML($body);
        $html = str_replace('<body>', '', $html);
        $html = str_replace('</body>', '', $html);
        $this->content = $html;
    }

    public function render(){
        $this->parse();

        if ( function_exists( 'mb_convert_encoding' ) ) {
            $content = mb_convert_encoding( $this->content, 'HTML-ENTITIES', get_option( 'blog_charset' ) );
        } else {
            $content = htmlspecialchars_decode( utf8_decode( htmlentities( $this->content, ENT_COMPAT, 'utf-8', false ) ) );
        }
        $content = str_replace("localhost:8080", "titus-karlen1993.rhcloud.com", $content);
        echo $content;
    }
}
