<!doctype html>
<html lang="<?php echo esc_attr( get_bloginfo( 'language' ) ); ?>" prefix="op: http://media.facebook.com/op#">
<head>
	<meta charset="<?php echo esc_attr( get_option( 'blog_charset' ) ); ?>">
	<link rel="canonical" href="<?php print_r(esc_url( $this->get_canonical_url() ) ); ?>">
	<meta property="op:markup_version" content="v1.0">
	<meta property="og:image" content="<?php echo $this->OGImage; ?>">
	<meta property="fb:article_style" content="<?php echo $this->style; ?>">
</head>
<body>
<article>
	<header>
		<?php
		$kicker = $this->category;
		if(!empty($kicker)){
			echo '<h3 class="op-kicker">'.$kicker.'</h3>';
		}
		?>
		<h1><?php echo esc_html( $this->get_the_title() ); ?></h1>
		<?php

		if($this->has_subtitle()){
			?>
			<h2><?php echo esc_html( $this->get_the_subtitle() ); ?></h2>
			<?php
		}
		$this->set_cover();
		?>

		<?php $authors = $this->get_the_authors() ?>
		<?php if ( is_array( $authors ) && count( $authors ) ) {?>
			<?php foreach ( $authors as $author ) { ?>
				<address>
					<?php
					$attributes = '';
					if ( strlen( $author->user_url ) ) {
						$attributes = ' href="' . esc_url( $author->user_url ) . '"';

						if ( isset( $author->role_contribution ) && strlen( $author->role_contribution ) ) {
							$attributes .= ' title="' . esc_attr( $author->role_contribution ) . '"';
						}

						if ( isset( $author->user_url_rel ) && strlen( $author->user_url_rel ) ) {
							$attributes .= ' rel="' . esc_attr( $author->user_url_rel ) . '"';
						}
					}
					?>
					<a<?php echo $attributes; ?>>
						<?php echo esc_html( $author->display_name ); ?>
					</a>
					<?php if ( strlen( $author->bio ) ) { ?>
						<?php echo esc_html( $author->bio ); ?>
					<?php } ?>
				</address>
			<?php } ?>
		<?php } ?>
		<time class="op-published" datetime="<?php echo esc_attr( $this->get_the_pubdate_iso() ); ?>">
			<?php echo esc_html( $this->get_the_pubdate() ); ?>
		</time>

		<time class="op-modified" datetime="<?php echo esc_attr( $this->get_the_moddate_iso() ); ?>">
			<?php echo esc_html( $this->get_the_moddate() ); ?>
		</time>
	</header>
	<?php
	$this->the_content();
	$this->set_map();
	$this->set_slider();
	?>
</article>
</body>
</html>