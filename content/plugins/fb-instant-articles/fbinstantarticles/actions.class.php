<?php
namespace FbInstantArticles;

/**
 * Class Actions
 * @package FbInstantArticles
 */
class Actions {
    protected  $errors;

    public function __construct()
    {
    }


    protected function show_errors(){
        return $this->errors;
    }

    public function publish($posts, $is_preview) {
        date_default_timezone_set('Australia/Sydney');
        foreach($posts as $postID) {
            $postID = (int)$postID;
            $post = get_post($postID);
            $article = new \FbInstantArticles\Instant_Articles_Post($post);
            $api = new \FbInstantArticles\API();
            $resp = $api->publish($article ,$is_preview);
            if($resp != false){
                $modifiedAt = date('Y/m/d h:i:s', time());
                $publish_status = $is_preview == false ? "Published" : "Preview";
                update_post_meta($postID, 'instant_articles_update_time', $modifiedAt);
                update_post_meta($postID, 'instant_articles_publish_status', $publish_status);
            }else{

            }
        }
        return $this->show_errors();
    }

    public function update($posts) {
        date_default_timezone_set('Australia/Sydney');

        foreach($posts as $postID) {
            $postID = (int)$postID;
            $post = get_post($postID);
            $article = new \FbInstantArticles\Instant_Articles_Post($post);
            $api = new \FbInstantArticles\API();
            $resp = $api->publish($article ,false);
            if($resp != false){
                $modifiedAt = date('Y/m/d h:i:s', time());
                $publish_status =  "Published";
                update_post_meta($postID, 'instant_articles_update_time', $modifiedAt);
                update_post_meta($postID, 'instant_articles_publish_status', $publish_status);
            }else{

            }

        }
        return $this->show_errors();

    }

    public function delete($posts) {

        foreach($posts as $postID) {
            $postID = (int)$postID;
            delete_post_meta($postID, 'instant_articles_update_time');
            delete_post_meta($postID, 'instant_articles_publish_status');
            $req = new \FbInstantArticles\API();
            $postsStatus = get_post_meta($postID, 'instant_articles_publish_status');
            $postsStatus = $postsStatus[0];
            $is_preview = true;
            if($postsStatus == 'Published'){
                $is_preview = false;
            }
            $req->delete($postID, $is_preview);



        }
        return $this->show_errors();
    }


    public function download($posts){
        header( 'Content-Type: text/html' );
        header( 'Content-Disposition: attachment; filename="article-' . absint( $posts[0] ) . '.txt"' );
        ob_clean();
        flush();
        $post = (int)$posts[0];
        $post = get_post($post);
        $article = new \FbInstantArticles\Instant_Articles_Post($post);
        $article = $article->to_instant_article();
        echo $article->render();
        exit;
    }

}
