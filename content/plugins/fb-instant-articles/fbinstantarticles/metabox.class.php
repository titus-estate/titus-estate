<?php
namespace FbInstantArticles;
/**
 * Render metabox in posts edit page
 * Class Metabox
 * @package FbInstantArticles
 */
class Metabox {

    protected $all_post_types  = array(
        'post',
    ),
        $errors = array(),
        $post;


    public function addAjaxActions() {
        add_action('wp_ajax_instant_articles_publish', array(&$this, 'publish'));
        add_action('wp_ajax_instant_articles_update', array(&$this, 'update'));
        add_action('wp_ajax_instant_articles_delete', array(&$this, 'delete'));
        add_action('wp_ajax_instant_articles_download', array(&$this, 'download'));

    }

    public function __construct() {

    }

    protected function show_errors($id){
        die(json_encode(array("data"=>$this->render($id, $this->errors)), true));
    }

    public function showMetaBox(){
        global $post;
        $this->post = $post;

        if($this->post->post_status !== 'publish'){
            return;
        }


        add_meta_box(
            'add_to_instant_articles',
            'Publish to instant articles',
            array( &$this, 'render' ),
            $this->all_post_types,
            'side',
            'high'
        );

        wp_enqueue_script( 'nstant_articles_js', plugins_url( 'fbinstantarticles/assets/instantarticles.js', dirname(__FILE__) ), array('jquery'), '1.0.0', true );
        wp_enqueue_style( 'instant_articles_css', plugins_url( 'fbinstantarticles/assets/instantarticles.css', dirname(__FILE__) ));

    }


    public function publish() {
        require plugin_dir_path( __FILE__ ) . '../vendor/autoload.php';
        $id = (int)$_GET['id'];
        $posts = array(
            $id
        );

        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->publish($posts, false);
        $this->show_errors($id);
    }


    public function update(){
        require plugin_dir_path( __FILE__ ) . '../vendor/autoload.php';
        $id = (int)$_GET['id'];
        $posts = array(
            $id
        );

        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->publish($posts, false);
        $this->show_errors($id);

    }

    public function delete(){
        require plugin_dir_path( __FILE__ ) . '../vendor/autoload.php';
        $id = (int)$_GET['id'];

        $posts = array(
            $id
        );

        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->delete($posts);
        $this->show_errors($id);

    }

    public function download(){
        require plugin_dir_path( __FILE__ ) . '../vendor/autoload.php';
        $id = (int)$_GET['id'];

        $posts = array(
            $id
        );

        $action = new \FbInstantArticles\Actions();
        $this->errors = $action->download($posts);

    }


    public function render($post_id_ajax) {


        if(!is_object($post_id_ajax) && !is_array($post_id_ajax)){
            $this->post = get_post($post_id_ajax);
        }

        $html = "";
        if(!empty($this->errors)) {

            foreach ($this->errors as $error) {
                $error_txt = "<p>Article-" . $error["ID"] . " </p> <p>Message- " . $error["text"] . "</p>";
                $html .=  '<div class="error notice">' . $error_txt . '</div>';
            }
        }

        $status = get_post_meta($this->post->ID, 'instant_articles_publish_status');
        $status = $status[0];
        if(empty($status)){
            $status = "Not published";
        }

        $html .=  "<div class='channel_wrap' ><div class='instant_articles_wait_overlay'></div> ";

        $html .=  '<div class="misc-pub-section">
                    <span style="color: #80858A" class="dashicons-before dashicons-align-left"></span>
                    <span>Page: <b>Titus Estate</b></span>
                    </div>
                    <div class="misc-pub-section" id="visibility">
                    <span>Status: <b>'.$status.'</b></span>
                    </div>';

        if($status == "Not published")
        {
            $html .=  '
                    <div class="misc-pub-section">
                        <a href="javascript:void(0);" data-id="'.$this->post->ID.'" class="instant_articles_publish">Publish</a>&nbsp;&nbsp;&nbsp;
                    </div>
                    <hr>';
        }else if($status == "Preview"){
            $instant_articles_update = get_post_meta($this->post->ID, 'instant_articles_update_time');
            $instant_articles_update = str_replace("T", " ", $instant_articles_update);
            $instant_articles_update = str_replace("Z", " ", $instant_articles_update);
            $html .=  '

                    <div class="misc-pub-section curtime" >
                     <span id="timestamp">Update time: <b>'.$instant_articles_update[0].'</b></span>
                    </div>

                    <div class="misc-pub-section">
                        <a href="javascript:void(0);" data-id="'.$this->post->ID.'" class="instant_articles_publish">Publish</a>&nbsp;&nbsp;&nbsp;
                        <a href="javascript:void(0);" style="color: #a00;"  data-id="'.$this->post->ID.'" class="instant_articles_delete">Delete</a>
                    </div>
                    <hr>';
        }elseif($status == "Published"){
            $instant_articles_update = get_post_meta($this->post->ID, 'instant_articles_update_time');
            $instant_articles_update = str_replace("T", " ", $instant_articles_update);
            $instant_articles_update = str_replace("Z", " ", $instant_articles_update);
            $html .=  '

                    <div class="misc-pub-section curtime" >
                     <span id="timestamp">Update time: <b>'.$instant_articles_update[0].'</b></span>
                    </div>

                    <div class="misc-pub-section">
                        <a href="javascript:void(0);"  data-id="'.$this->post->ID.'" class="instant_articles_update">Update</a>&nbsp;&nbsp;&nbsp;
                        <a href="javascript:void(0);" style="color: #a00;" data-id="'.$this->post->ID.'" class="instant_articles_delete">Delete</a>
                    </div>
                    <hr>';
        }
        $html .=  "</div>";
        $html .=  '<div style="text-align:center"><a href="javascript:void(0);"  data-id="'.$this->post->ID.'" class="instant_articles_download">Download</a></div>';

        if(!is_object($post_id_ajax) && !is_array($post_id_ajax)){
            return $html;
        }else{
            echo $html;
        }

    }
}