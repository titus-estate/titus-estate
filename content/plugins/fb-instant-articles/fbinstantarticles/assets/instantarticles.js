var $ = jQuery;
$(document).ready(function(){
    attach_instant_articles_events();
});


function  attach_instant_articles_events(){

    $('.instant_articles_publish').on('click', function(){
        var post_id = $(this).attr('data-id');
        var channel = $(this).attr('data-channel');
        var action = 'publish';
        instant_articles_action(post_id, channel, action);
    });

    $('.instant_articles_publish_as_preview').on('click', function(){
        var post_id = $(this).attr('data-id');
        var channel = $(this).attr('data-channel');
        var action = 'publish_as_preview';
        instant_articles_action(post_id, channel, action);
    });

    $('.instant_articles_update').on('click', function(){
        var post_id = $(this).attr('data-id');
        var channel = $(this).attr('data-channel');
        var action = 'update';
        instant_articles_action(post_id, channel, action);
    });

    $('.instant_articles_delete').on('click', function(){
        var post_id = $(this).attr('data-id');
        var channel = $(this).attr('data-channel');
        var action = 'delete';
        instant_articles_action(post_id, channel, action);
    });

    $('.instant_articles_download').on('click', function(){
        var ajax_url = '/wordpress/wp-admin/admin-ajax.php';
        var post_id = $(this).attr('data-id');
        var action = 'download';
        var url = ajax_url+'?action=instant_articles_download&channel=0&id='+post_id;
        window.location.href = url;
    });



}


function instant_articles_action(id, channel, action){
    var ajax_url = '/wordpress/wp-admin/admin-ajax.php';
    var wait_wrap = $('.channel_wrap[data-channel="'+channel+'"] .instant_articles_wait_overlay');
    wait_wrap.show();
    $.ajax({
        type : 'GET',
        dataType : 'json',
        url : ajax_url,
        data : {
            action: 'instant_articles_'+action,
            channel: channel,
            id : id
        },
        success : function(response) {
            var data = response.data || false;
            if(data != false){
                $('#add_to_instant_articles.postbox > .inside').html(data);
            }
            wait_wrap.hide();
            attach_instant_articles_events();
        }
    });
}