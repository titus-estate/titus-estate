<?php
namespace FbInstantArticles;
/**
 * Class Settings
 * @package FbInstantArticles
 */
class Settings {

    public $app_id = '315388171952167';
    public $app_secret = 'a32a205041382669a49911050f30764d';
    public $page_id = '1056443281060395';
    public $token = false;

    public function __construct(){
        $this->setTokens();
        $this->doAction();
    }

    public function doAction(){
        if(isset($_GET['page']) && $_GET['page'] == 'fb_instant_articles_settings')
        {
            $action = $_GET['action'];
            switch($action){
                case 'flush' :
                    $page = isset($_GET['channel']) ? $_GET['channel'] : 'all';
                    $this->flush_token($page);
                    break;
                case 'get_tokens':
                    $this->getTokens();
                    break;
                default:
                    return ;
            }
        }

    }

    public function setTokens(){
        $token = get_option('facebook_instant_articles_aceess_token');
        if($token && !empty($token)){
            $this->token = $token;
        }
    }

    protected function flush_token($page = 'all'){
        delete_option('facebook_instant_articles_aceess_token');
        $redirectUrl = menu_page_url('fb_instant_articles_settings', false);
        echo("<script>location.href = '$redirectUrl';</script>");
        exit();
    }

    protected function getLongLivedTokens(){
        $redirectUrl = menu_page_url('fb_instant_articles_settings', false).'&action=get_tokens&pg=instant_articles';
        $fb = new \Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => 'v2.5',
        ]);
        $helper = $fb->getRedirectLoginHelper();

        if(!isset($_GET['code'])){
            $permissions = ['email', 'pages_manage_instant_articles', 'pages_show_list'];
            $loginUrl = $helper->getLoginUrl($redirectUrl, $permissions);
            echo("<script>location.href = '$loginUrl';</script>");
            exit();
        }else{

            try {
                $accessToken = $helper->getAccessToken();
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo $e->getMessage();
                exit;
            }

            try {
                $pageToken = $fb->get('/oauth/access_token?grant_type=fb_exchange_token&client_id='.
                    $this->app_id.
                    '&client_secret='.$this->app_secret.
                    '&fb_exchange_token='.$accessToken->getValue(), $accessToken->getValue());
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'ERROR: Graph ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo $e->getMessage();
                exit;
            }

            try {
                $response = $fb->get('/me?fields=id', $accessToken->getValue());
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'ERROR: Graph ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo $e->getMessage();
                exit;
            }
            $me = $response->getGraphUser();
            $us_id = $me->getId();

            try {
                $pages = $fb->get('/'.$us_id.'/accounts', $pageToken->getAccessToken());
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'ERROR: Graph ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo $e->getMessage();
                exit;
            }

            $data = $pages->getDecodedBody();
            $file = plugin_dir_path(__FILE__).'wp_fb_response.txt';
            if(!file_exists($file)) {
                @file_put_contents($file, print_r($data, true));
            }
            $data = ( isset($data['data']) && !empty($data['data']) ) ? $data['data'] : array();

            if(empty($data)){
                var_dump('Error empty data...');
                exit();
            }

            $options_data = false;

            foreach($data as $value){
                $access = $value['access_token'];
                $id = $value['id'];

                if($id == $this->page_id){
                    $options_data = $access;
                }
            }

            return $options_data;

        }
    }

    protected function getTokens(){

        $pageAccessToken = $this->getLongLivedTokens();
        update_option('facebook_instant_articles_aceess_token', $pageAccessToken);
        $redirectUrl = menu_page_url('fb_instant_articles_settings', false);
        echo("<script>location.href = '$redirectUrl';</script>");
        exit();

    }

    public  function render() {
        ?><div class="wrap">


        <h2>Facebook Instant Articles settings</h2>

        <?php

        $errors = array();
        $success = array();


        $token = get_option('facebook_instant_articles_aceess_token');
        if(!$token || empty($token)){
            $errors[] = 'invalid';
        }else{
            $success[] = 'valid';
        }

        $html = "";

        if(isset($errors)){

            if(!empty($success)) {
                foreach ($success as $succes) {
                    $success_txt = "<p>Access token is valid</p>";
                    $html .=  '<div class="updated notice">' . $success_txt . '</div>';
                }
            }

            if(!empty($errors)) {
                foreach ($errors as $error) {
                    $error_txt = "<p>Invalid access token</p>";
                    $html .=  '<div class="error notice">' . $error_txt . '</div>';
                }
            }

        }


        echo $html;
        ?>

        <form method="get">
            <input type="hidden" name="page" value="fb_instant_articles_settings">
            <input type="hidden" name="action" value="get_tokens">
            <input type="submit" style="float: left;" class="button button-primary button-large" value="Update tokens">
        </form>

        <form method="get">
            <input type="hidden" name="page" value="fb_instant_articles_settings">
            <input type="hidden" name="action" value="flush">
            <input type="submit" style="float: left; margin-left: 10px;" class="button button-primary button-large" value="Flush">
        </form>


        </div>
        <?php
    }

}