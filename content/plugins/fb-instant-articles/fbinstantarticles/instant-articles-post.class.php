<?php
namespace FbInstantArticles;

class Instant_Articles_Post{

    protected $_post = null;

    protected $_postData = null;

    protected $_content = null;

    protected $_subtitle = null;

    protected $OGImage = null;

    protected $style = 'default';

    protected $category;

    public function __construct( $post ) {
        $this->_post = $post;
        $this->_postData = get_fields($post->ID);

        $this->set_the_kicker();
    }

    public function set_map(){
        $address = $this->_postData['address'];
        $data = array(
            "type" => "Feature",
            "geometry" => array(
                "type" => "Point",
                "coordinates" => array(
                    $address['lat'],
                    $address['lng'],
                )
            ),
            "properties" => array(
                "title" => $address['address'],
                "radius" =>  7500,
                "pivot" => true,
                "style" => "satellite"
            ),
        );
        $json = json_encode($data);
        $map =
            '<figure class="op-map">'.
            '<script type="application/json" class="op-geotag">'.
            $json.
            '</script>'.
            '<figcaption class="op-vertical-below">'.
            '<h1>'.$address['address'].'</h1>'.
            '</figcaption>'.
            '</figure>';
        echo $map;
    }

    public function get_the_id() {
        return $this->_post->ID;
    }

    public function get_the_title() {
        $title = $this->_post->post_title;

        return $title;
    }

    public function has_subtitle() {
        $subtitle = $this->get_the_subtitle();
        if ( ! empty( $subtitle ) && strlen( $subtitle )  ) {
            return true;
        }
        return false;
    }

    public function get_the_subtitle() {
        $excerpt = empty( $this->_postData['description'] ) ? $this->_post->post_excerpt : $this->_postData['description'];
        return $excerpt;
    }

    public function get_canonical_url() {
        // If post is draft, clone it to get the eventual permalink,
        // see http://wordpress.stackexchange.com/a/42988.
        if ( in_array( $this->_post->post_status, array( 'draft', 'pending', 'auto-draft' ), true ) ) {
            $post_clone = clone $this->_post;
            $post_clone->post_status = 'published';
            $post_clone->post_name = sanitize_title( $post_clone->post_name ? $post_clone->post_name : $post_clone->post_title, $post_clone->ID );
            $url = get_permalink( $post_clone );
        } else {
            $url = get_permalink( $this->_post );
        }
        return $url;
    }

    public function get_the_content() {

        global  $post;
        $post = $this->_post;
        $content = new \FbInstantArticles\Content($this->_post->post_content);
        return $content;
    }

    public function the_content(){
        $content = $this->get_the_content();
        $content->render();
    }

    public function get_the_pubdate_iso() {
        $published_date = mysql2date( 'c', get_post_time( 'Y-m-d H:i:s', true, $this->_post->ID ), false );
        return $published_date;
    }

    public function get_the_pubdate() {
        $published_date = get_post_time( get_option( 'date_format' ), true, $this->_post->ID);
        return $published_date;
    }

    public function get_the_moddate_iso() {

        $modified_date = mysql2date( 'c', get_post_modified_time( 'Y-m-d H:i:s', true, $this->_post->ID ), false );
        return $modified_date;

    }

    public function get_the_moddate() {

        $modified_date = get_post_modified_time( get_option( 'date_format' ), true, $this->_post->ID);
        return $modified_date;

    }

    public function set_the_kicker(){
        $post_categories = get_the_category( $this->_post->ID );
        if ( ! empty( $post_categories ) ) {
            $category = $post_categories[0];

            if ( $category->parent > 0 ) {
                $category = get_category( $category->parent );
            }
        }
        $this->category = $category->name;
    }

    public function get_the_authors() {

        $authors = array();

        $wp_user = get_userdata( $this->_post->post_author );

        if ( is_a( $wp_user, 'WP_User' ) ) {
            $author = new \stdClass;
            $author->ID            = $wp_user->ID;
            $author->display_name  = $wp_user->data->display_name;
            $author->first_name    = $wp_user->first_name;
            $author->last_name     = $wp_user->last_name;
            $author->user_login    = $wp_user->data->user_login;
            $author->user_nicename = $wp_user->data->user_nicename;
            $author->user_email    = $wp_user->data->user_email;
            $author->user_url      = $wp_user->data->user_url;
            $author->bio           = $wp_user->description;

            $authors[] = $author;
        }

        return $authors;
    }

    public function get_cover_data() {


        $_article_banners = get_post_slider_images( $this->_post->ID );
        $article_banners = array();
        foreach($_article_banners as $value) {
            $article_banners[] = $value;
        }

        return $article_banners;
    }

    public function set_slider(){
        $image_data = $this->get_cover_data();
        if( count($image_data) > 1) {
            unset($image_data[0]);
            $slideShow = '<figure class="op-slideshow">';
            foreach($image_data as $cover_image){
                $image ='<figure><img src="'.$cover_image.'"/>';
                $image .= '</figure>';
                $slideShow .= $image;
            }

            $slideShow .= '</figure>';
            echo $slideShow;
        }
    }

    public function set_cover(){
        $image_data = $this->get_cover_data();
        if(!empty($image_data)){
            $cover = $image_data[0];
            $this->OGImage = $cover['url'];
            $image ='<figure><img src="'.$cover.'"/>';
            $image .= '</figure>';
            echo $image;

        }
    }

    public function to_instant_article() {
        return $this;
    }

    public function render(){
        ob_start();
        $default_template = dirname( __FILE__ ) . '/template.php';
        include $default_template;
        $html  = ob_get_contents();
        libxml_use_internal_errors( true );
        $document = new \DOMDocument();
        $document->preserveWhiteSpace = true;
        $document->formatOutput = false;
        $document->encoding = 'utf8';
        $document->loadHTML('<?xml encoding="UTF-8">' . $html);
        $html = $document->saveHTML();
        $html = str_replace('<?xml encoding="UTF-8">', '', $html);
        ob_end_clean();
        $html = str_replace("localhost:8080", "titus-karlen1993.rhcloud.com", $html);
        return $html;
    }

}
