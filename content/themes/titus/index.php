<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.WordPress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Titus
 */
get_header();
load_module('Slider');
load_module('Search');
load_module('Latest');
load_module('Follow');
load_module('Banner', array());
load_module('Comments');
load_module('Testimonials');
load_module('Banner', array());
get_footer();
