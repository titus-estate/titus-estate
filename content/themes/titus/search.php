<?php
/**
 * The template for displaying Search result pages.
 *
 * @package WordPress
 * @subpackage Titus
 */

get_header();
load_module('FullSearch');
load_module('Banner', array());
load_module('Latest');
load_module('Follow');
load_module('Comments');
load_module('Testimonials');
load_module('Banner', array());
get_footer();
