<?php
/**
 * @package WordPress
 * @subpackage Titus
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html id="ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <?php
    if ( is_front_page() ) {
        ?>
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?php echo bloginfo( 'name' ); ?>">
        <meta property="og:url" content="<?php echo( home_url( '/' ) ); ?>">
        <meta property="og:image" content="<?php echo( home_url( '/' ) ); ?>content/themes/titus/assets/images/logo.png">
        <meta property="og:description" content="<?php bloginfo( 'description' );  ?>">
        <title><?php echo bloginfo( 'name' ); ?></title>
        <meta name="description" content="<?php bloginfo( 'description' );  ?>">
        <?php
    }elseif ( is_singular() ) {
        global $post;
        ?>
        <meta property="og:type" content="article">
        <meta property="og:title" content="<?php echo( esc_attr( $post->post_title ) ); ?>">
        <?php
        $featured_image_src = get_post_slider_images( $post->ID );

        if ( ! empty( $featured_image_src ) ) {
            ?>
            <meta property="og:image" content="<?php echo( $featured_image_src[0] ); ?>">
            <?php
        }
        ?>
        <meta property="og:url" content="<?php echo( get_permalink( $post->ID ) ); ?>">
        <meta property="og:description" content="<?php echo( esc_attr( $post->post_excerpt ) ); ?>">
        <meta name="description" content="<?php echo( esc_attr( $post->post_excerpt ) ); ?>">
        <meta name="keywords" content="<?php echo  tags_to_string($post->ID); ?>">
        <?php
    } elseif(is_page()) {
        global $post;
        ?>
        <meta property="og:type" content="article">
        <meta property="og:title" content="<?php echo( esc_attr( $post->post_title ) ); ?>">
        <?php
        $featured_image_src = get_post_slider_images( $post->ID );

        if ( ! empty( $featured_image_src ) ) {
            ?>
            <meta property="og:image" content="<?php echo( $featured_image_src[0] ); ?>">
            <?php
        }
        ?>
        <meta property="og:url" content="<?php echo( get_permalink( $post->ID ) ); ?>">
        <meta property="og:description" content="<?php echo( esc_attr( $post->post_excerpt ) ); ?>">
        <meta name="description" content="<?php echo( esc_attr( $post->post_excerpt ) ); ?>">
        <meta name="keywords" content="<?php echo  tags_to_string($post->ID); ?>">
        <?php
    }
    ?>



    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/build/app.min.css" />
    <?php wp_head(); ?>
</head>
<body>
<div id="fb-root"></div>
<div class="header_block">
    <?php
    load_module('HeaderContacts');
    load_module('SocialShare');
    load_module('Navigation');
    ?>
</div>
<div class="fixed-header_block"></div>