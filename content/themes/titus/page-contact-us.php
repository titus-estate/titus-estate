<?php
/**
 * The template for displaying contact us page.
 *
 * @package WordPress
 * @subpackage Titus
 */
get_header();
load_module('ContactUsMap');
?>
    <div class="container page-container-main">
        <div class="row">
            <div class="col l8 s12 m6 left_page_content">
                <?php
                load_module('ContactUsPage');
                load_module('ContactUsForm');
                ?>
            </div>

            <div class="col l4 s12 m6 right_sidebar">
                <?php
                    load_module('RightSidebar');
                ?>
            </div>
        </div>
    </div>
<?php
load_module('Banner', array());
load_module('Latest');
load_module('Follow');
load_module('Banner', array());
get_footer();
