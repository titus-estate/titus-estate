class Map {

    constructor(el, coordinates) {

        this.maps = []
        this.apiUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAbIdx3615VVAqjHnyLlxLtuY0a6zNL9HQ&v=3.exp&sensor=false&callback='
        this.apiIsRequested = false
        this.apiIsLoaded = false
        this.icon = '/content/themes/titus/assets/images/marker.png'
        this.id = 'ttsm-' + 0 + '-' + (+new Date())
        this.el = el
        this.coordinates = coordinates || []
        this.map = null
        this.markers = {}
        this.geoMarker = null
        this.isInit = false
    }

    ApiIsLoaded() {
        this.apiIsLoaded = true;
        PubSub.publish('onGoogleMapApiReady');
    }

    createMap() {
        var _self = this

        if(window.GMAPAPILOADED == true){
            _self.ApiIsLoaded();
            return true;
        }

        if (!_self.apiIsLoaded && !_self.apiIsRequested && !window.GMAPAPIREQUESTED) {
            _self.apiIsRequested = true;
            window.GMAPAPIREQUESTED = true

            var guid = 'fn' + (+new Date());
            window.__GMAP = []

            window.__GMAP[guid] = function () {
                _self.ApiIsLoaded();
                delete window.__GMAP;
                window.GMAPAPILOADED = true
            }

            var js = document.createElement('script');
            js.type = 'text/javascript';
            js.async = true;
            js.src = _self.apiUrl + 'window.__GMAP.' + guid;
            document.body.appendChild(js);
        }else if(window.GMAPAPIREQUESTED == true){
            PubSub.subscribe('onGoogleMapApiReady', function(){
                _self.apiIsLoaded = true;
            });
        }
    }

    render(){
        var _self = this;
        var zeroCoordinates = _self.getCityCoordinates()
        if (zeroCoordinates) {
            var latLang = new google.maps.LatLng(zeroCoordinates.latitude, zeroCoordinates.longitude);
            this.map = new google.maps.Map(this.el, this.getMapOptions(latLang));
            this.renderMarkers();

            google.maps.event.addListener(this.map, 'dragend', () => {
                var center = _self.map.getCenter();
                PubSub.publish('onGoogleMapDragEnd', {
                    id: _self.id,
                    latitude: center.lat(),
                    longitude: center.lng()
                });
            })


            google.maps.event.addListener(this.map, 'bounds_changed', () => {
                var center = _self.map.getCenter();
                PubSub.publish('onGoogleMapZoomChange', {
                    id: _self.id,
                    latitude: center.lat(),
                    longitude: center.lng()
                });
            })

            PubSub.subscribe('window.resize', function () {
                _self.resize();
            })

        }

        this.isInit = true;
    }

    getMapOptions(latLng) {

        return {
            zoom : 3,
            center : latLng,
            backgroundColor : '#f2f2f2',
            disableDefaultUI : true,
            draggable :  true,
            scrollwheel : false,
            styles : [
                {
                    featureType : 'all',
                    elementType : 'all',

                },
                {
                    featureType : 'landscape',
                    elementType : 'geometry',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#E0EFEF'
                    }]
                },
                {
                    featureType : 'water',
                    elementType : 'geometry',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#64B5F6'
                    }]
                },
                {
                    featureType : 'all',
                    elementType : 'labels.text.fill',
                    stylers : [{
                        visibility : 'on'
                    },
                        {
                            color : '#000000'
                        }]
                }, {
                    featureType : 'all',
                    elementType : 'labels.text.stroke',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#ffffff'
                    }, {
                        weight : 2
                    }]
                }, {
                    featureType : 'poi',
                    elementType : 'labels.icon',
                    stylers : [{
                        visibility : 'simplified'
                    }]
                }, {
                    featureType : 'road',
                    elementType : 'geometry.fill',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#ffffff'
                    }]
                }, {
                    featureType : 'poi',
                    elementType : 'geometry',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#C0E8E8'
                    }]
                }, {
                    featureType : 'road',
                    elementType : 'geometry.stroke',
                    stylers : [{
                        color: '#3C90BE'
                    }]
                }, {
                    featureType : 'transit',
                    elementType : 'labels.icon',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        invert_lightness : true
                    }, {
                        hue : '#ffffff'
                    }, {
                        saturation : -100
                    }, {
                        lightness : -21
                    }, {
                        gamma : 1.5
                    }]
                }, {
                    featureType : 'poi',
                    elementType : 'labels',
                    stylers : [{
                        visibility : 'on'
                    }]
                }, {
                    featureType : 'transit',
                    elementType : 'geometry.fill',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#75A7D8'
                    }, {
                        weight : 1
                    }]
                }, {
                    featureType : 'transit',
                    elementType : 'geometry.fill',
                    stylers : [{
                        visibility : 'on'
                    }, {
                        color : '#75A7D8'
                    }]
                }, {
                    featureType : 'administrative.land_parcel',
                    elementType : 'geometry',
                    stylers : [{
                        color: '#6698CD',
                        fill: 'white'
                    }]
                }, {
                    featureType : 'administrative.land_parcel',
                    elementType : 'labels',
                    stylers : [{
                        visibility : 'off'
                    }]
                }, {
                    featureType : 'road',
                    elementType : 'labels.icon',
                    stylers : [{
                        visibility : 'off'
                    }]
                }, {
                    featureType : 'poi',
                    elementType : 'labels.icon',
                    stylers : [{
                        visibility : 'off'
                    }]
                }]
        }

    }

    getCityCoordinates() {
        var coordinates = {
            latitude : 40.7520482,
            longitude : -74.0253677
        };
        return coordinates;
    }

    init() {
        var _self = this;

        _self.createMap()

        if (_self.apiIsLoaded) {
            this.render();
        } else {
            PubSub.subscribe('onGoogleMapApiReady', function() {
                _self.render();
            });
        }
        return this;
    }

    renderMarkers() {
        var _self = this;
        this.coordinates.map((item) => {
            _self.renderMarker(item);
        })

        if (this.coordinates.length === 1) {
            this.map.setCenter({
                lat : parseFloat(this.coordinates[0].latitude),
                lng : parseFloat(this.coordinates[0].longitude)
            });
        } else if (this.coordinates.length > 0) {
            this.fitMarkers();
        }
    }

    fitMarkers() {
        var _self = this;
        var bounds = new google.maps.LatLngBounds();
        $.each(_self.markers, function (index, marker) {
            bounds.extend(marker.getPosition());
        })
        this.map.setCenter(bounds.getCenter());
        this.map.fitBounds(bounds);
    }

    renderMarker(item) {
        var _self = this,
            markerId = item.latitude + '' + item.longitude;

        if (this.markers[markerId]) {
            return null
        }
        var makerIcon = _self.icon
        if(item.icon){
            makerIcon = item.icon
        }
        var markerImage = new google.maps.MarkerImage(makerIcon, null, null, null, new google.maps.Size(44, 48))
        var latLang = new google.maps.LatLng(item.latitude, item.longitude)
        var marker = this.markers[markerId] = new google.maps.Marker({
            position : latLang,
            map : this.map,
            icon : markerImage
        });

        if (item.url && item.url !== '') {
            google.maps.event.addListener(marker, 'click', () => {
                window.location.href = item.url
            })
        }

        return marker;
    }

    checkLocationInBounds($lat, $lng) {
        var bounds = this.map.getBounds();
        var latLng = new google.maps.LatLng($lat, $lng)
        return bounds.contains(latLng);
    }

    clearMarkers() {
        var _self = this
        $.each(_self.markers, function (index, marker) {
            marker.setMap(null)
        })
        this.markers = {}
    }


    updateMarkers(coordinates) {
        this.clearMarkers()

        this.coordinates = coordinates
        console.log(this.apiIsLoaded)
        if (this.apiIsLoaded) {
            this.renderMarkers()
        }
    }

    addMarkers(coordinates) {
        var _self = this
        coordinates.map((item) => {
            var markerId = item.latitude + '' + item.longitude;
            if (_self.markers[markerId]) {
                _self.coordinates.push(item);
            }

            if (_self.apiIsLoaded) {
                _self.renderMarker(item);
            }
        })
    }

    moveTo(la, lo) {
        this.renderMarker({
            latitude : la,
            longitude : lo
        })

        // Focus on the target marker.
        var latLang = new google.maps.LatLng(la, lo)
        this.map.setCenter(latLang)
    }

    moveToCurrentPosition(la, lo) {
        var _self = this
        var markerImage = new google.maps.MarkerImage(_self.icon, null, null, null, new google.maps.Size(28, 42));
        var latLang = new google.maps.LatLng(la, lo);
        this.map.setCenter(latLang);
        if (!this.geoMarker) {
            this.geoMarker = new google.maps.Marker({
                animation : google.maps.Animation.DROP,
                position : latLang,
                map : this.map,
                icon : markerImage
            });
        } else {
            this.geoMarker.setAnimation(google.maps.Animation.DROP);
        }
    }

    resize() {
        if (this.map && google) {
            google.maps.event.trigger(this.map, 'resize');
        }
    }

    zoomIn() {
        if (this.map && google) {
            this.map.setZoom(this.map.getZoom() + 1)
        }
    }

    zoomOut() {
        if (this.map && google) {
            this.map.setZoom(this.map.getZoom() - 1)
        }
    }

    zoomTo(zoom) {
        if (this.map && google) {
            this.map.setZoom(zoom)
        }
    }

    bounceMarker(markerId, bool) {
        if (!this.apiIsLoaded) {
            return;
        }

        var marker = this.markers[markerId];
        if (marker) {
            if (bool) {
                marker.setAnimation(google.maps.Animation.BOUNCE)
            } else {
                marker.setAnimation(null)
            }
        }
    }

}

module.exports = Map