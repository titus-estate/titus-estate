<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Titus
 */

get_header(); ?>
Page not found
<?php get_footer(); ?>