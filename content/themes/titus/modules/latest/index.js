class Latest  {

    constructor(){
        this.attachEvents()
    }


    attachEvents() {
        var _self = this
        PubSub.subscribe('document.ready', function() {
            $('.materialboxed').materialbox();
        });
    }
}

new Latest();