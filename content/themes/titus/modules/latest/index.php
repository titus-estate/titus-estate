<?php
namespace Modules;

class Latest extends  \Titus\Core\Module
{

    protected $limit = 4;

    protected function getLatest($type){
        $ret = array();
        $idObj = get_category_by_slug($type);
        $catID = $idObj->term_id;
        $args = array(
            'posts_per_page' => $this->limit,
            'category' => $catID,
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'post',
            'post_status'      => 'publish',
        );
        $posts = get_posts( $args );
        foreach($posts as $post) {
            $post_data = get_fields($post->ID);
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '600x400' );
            $img = $thumb[0];
            $name = $post->post_title;
            $url = get_permalink($post);
            $excerpt = $post->post_excerpt;
            $ret[] = array(
                "image" => $img,
                "title" => $name,
                "id" => $post->ID,
                "about" => $excerpt,
                "url" => $url,
                "bathrooms" => $post_data["bathrooms"],
                "rooms" => $post_data["rooms"],
                "price" => number_format($post_data["price"]),
                "ft" => $post_data["property_size"],
                "share" => get_module("Share", array(
                    "post_id" => $post->ID
                ))
            );
        }
        return $ret;
    }

    protected function setData(){

        $this->data = array(
            "sales" => $this->getLatest('sales'),
            "rents" => $this->getLatest('rentals')
        );
    }
}