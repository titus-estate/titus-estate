<div class="module_follow">
    <div class="parallax-container">
        <div class="parallax"><img src="/content/themes/titus/assets/images/parallax1.jpg"></div>
    </div>
    <div class="section">
        <div class="row">
            <div class="module_follow_form">
                <div class="title col l8 m10 s12 push-l2 push-m1">
                    Follow Our newsletter
                </div>
                <div class="col l12 m12 s12"></div>
                <div class="text col l8 m10 s12 push-l2 push-m1">
                    Be the first who know about best offers
                </div>
                <div class="col l12 m12 s12"></div>
                <div class="follow_form col l4 m8 s12 push-l4 push-m2">
                    <div class="col s12">
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col s12">
                                    <input class="input_email form-control" name="follow_email" id="follow_email" type="email" placeholder="E-mail address">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col s12">
                                    <i class="follow-submit btn waves-effect waves-light btn-large waves-input-wrapper"><input class="waves-button-input" id="submit" type="button" value="follow"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="/content/themes/titus/assets/images/parallax1.jpg"></div>
    </div>
</div>