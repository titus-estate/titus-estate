<?php
namespace Modules;

class Follow extends \Titus\Core\Module
{
    protected function create_table()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `follows` (
                `id` int(11) unsigned NOT NULL auto_increment,
                `email` varchar(255) NOT NULL default '',
                `is_new` int(11) NOT NULL default 1,
                PRIMARY KEY  (`id`)
        )ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        global $wpdb;
        $wpdb->query($sql);

    }

    public function follow() {
        global $wpdb;
        $this->create_table();
        if(!empty($_REQUEST['mail']) && filter_var($_REQUEST['mail'], FILTER_VALIDATE_EMAIL) ){
            $email = sanitize_email($_REQUEST['mail']);
            $is_follow = $wpdb->get_results("SELECT * FROM follows WHERE email = '$email' ");
            if(empty($is_follow)) {
                if($wpdb->insert('follows', array('email' => $_REQUEST['mail']))){
                    die('true');
                }else{
                    echo  die('error');
                }
            } else {
                echo die('true');
            }
        }else{
            echo die('false');
        }
    }
}