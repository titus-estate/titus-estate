import Validate from 'utils/validate'
import xhr from 'utils/xhr'

class Follow  {

    constructor(){
        this.attachEvents()
    }

    attachEvents() {
        PubSub.subscribe('document.ready', function(){
            $('.module_follow .module_follow_form .follow-submit').click(function(){
                var email = $('.module_follow .module_follow_form #follow_email').val();
                var validate = new Validate();
                var is_email = validate.hooks.valid_email(email);
                if(!is_email){
                    $('.module_follow .module_follow_form #follow_email').addClass('error');
                }else {
                    $('.module_follow .module_follow_form #follow_email').removeClass('error');
                    $('.module_follow .module_follow_form .follow-submit #submit').val('........');
                    xhr({
                        action: 'follow',
                        mail: email
                    },function(response){
                        $('.module_follow .module_follow_form .follow-submit #submit').val('FOLLOW');
                        if(response === true){
                            $('.module_follow .module_follow_form #follow_email').val('');
                            Materialize.toast('Success!', 4000);
                        }else if(response === false){
                            $('.module_follow .module_follow_form #follow_email').addClass('error');
                        }else {
                            Materialize.toast('Server Error!', 4000);
                        }
                    }, function(){
                        Materialize.toast('Server Error!', 4000);
                        $('.module_follow .module_follow_form .follow-submit #submit').val('FOLLOW');
                    });
                }
            });
        });
    }
}

new Follow();

