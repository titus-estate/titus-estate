class Share  {

    constructor(){
        this.attachEvents()
    }


    attachEvents() {
        $('.module_share > a').click(function(){
            if($(this).attr('data-href')) {
                window.open($(this).attr('data-href'), "", "width=320")
            }
        })
    }
}

new Share();
