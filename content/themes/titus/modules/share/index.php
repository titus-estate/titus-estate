<?php
namespace Modules;

class Share extends \Titus\Core\Module
{
    protected function setData(){
        global $post;
        if(!empty($this->args['post_id'])) {
            $post = get_post($this->args['post_id']);
        }
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '600x400' );
        $img = $thumb[0];
        $this->data = array(
            "url" => urlencode(get_permalink($post)),
            "img" => $img,
            "post" => $post
        );
    }
}