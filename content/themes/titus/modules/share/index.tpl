<div class="module_share">
        <a data-href="https://www.facebook.com/sharer/sharer.php?u={$url}"><i class="fa fa-facebook"></i>&nbsp;&nbsp;</a>
        <a data-href="http://twitter.com/home?status={$post->post_title}+{$url}"><i class="fa fa-twitter"></i>&nbsp;&nbsp;</a>
        <a data-href="https://plus.google.com/share?url={$url}"><i class="fa fa-google-plus"></i>&nbsp;&nbsp;</a>
        <a data-href="https://pinterest.com/pin/create/button/?url={$url}&media={$img}&description={$post->post_title}"><i class="fa fa-pinterest"></i>&nbsp;&nbsp;</a>
        <a href="mailto:?&body={$url}"><i class="fa fa-envelope"></i>&nbsp;&nbsp;</a>
        <a href="javascript:window.print()"><i class="fa fa-print"></i>&nbsp;&nbsp;</a>
</div>