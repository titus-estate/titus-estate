var Map = require("utils/map")
import xhr from 'utils/xhr'

class FullSearch  {

    constructor(){
        if($('.module_full_search').length > 0){

            this.map = false;
            this.itemTemplate = '';
            this.attachEvents()

        }
    }

    setMapHeight(){
        var _self = this;
        var height = $(window).height() - 65;
        $('.module_full_search .search').css('min-height', $(window).height());
        $('.module_full_search .map').css('height',height).stick_in_parent({
            spacer: false,
            offset_top: 65,
            recalc_every: 1
        });
        PubSub.subscribe('window.resize', () => {
            var height = $(window).height() - 65;
            $('body').trigger("sticky_kit:recalc");
            $('.module_full_search .map').css('height',height)
        });

        PubSub.subscribe('layoutChanged', () => {
            var height = $(window).height() - 65;
            $('body').trigger("sticky_kit:recalc");
            $('.module_full_search .map').css('height',height)
            _self.map.fitMarkers()
        });

    }

    loadMap(){

        var _self = this
        var coordinates = [];
        $('.module_full_search .search .items .card').each(function() {
            var coords = {
                'latitude' : $(this).attr('data-lat'),
                'longitude' : $(this).attr('data-lng'),
                'url' : $(this).attr('data-url')
            };
            var tm = 0;
            coordinates.push(coords);
            $(this).hover(function () {
                var markerId = $(this).attr('data-lat') + '' + $(this).attr('data-lng');
                _self.map.bounceMarker(markerId, true)
                tm = setTimeout(function(){
                    _self.map.bounceMarker(markerId, false)
                }, 1500)
            }, function () {
                clearTimeout(tm);
                var markerId = $(this).attr('data-lat') + '' + $(this).attr('data-lng');
                _self.map.bounceMarker(markerId, false)
            }).click(function () {
                _self.map.moveTo($(this).attr('data-lat'), $(this).attr('data-lng'))
                _self.map.zoomTo(15)
            })
        });

        if(!this.map) {
            this.map =  new Map($('.module_full_search .map').get(0), coordinates)
            this.map.init()
        }else {
            this.map.updateMarkers(coordinates);
        }

    }

    toprice(x){
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return parts.join(".");
    }

    empty(str) {
        return (!str || 0 === str.length);
    }

    getItemBathrooms(item){
        if(this.empty(item.bathrooms)){
            return '';
        }
        return `
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/bathroom.png" alt="Contact Person">
            Bathooms<span class="new badge">${item.bathrooms}</span>
        </div>
        `;
    }
    getItemRooms(item){
        if(this.empty(item.rooms)){
            return '';
        }
        return `
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/bed.png" alt="Contact Person">
            Rooms<span class="new badge">${item.rooms}</span>
        </div>
        `;
    }

    getItemFt(item){
        if(this.empty(item.ft)){
            return '';
        }
        return `
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/home.png" alt="Contact Person">
            ${item.ft} ft2</span>
        </div>
        `;
    }

    getItemPrice(item){
        if(this.empty(item.price)){
            return '';
        }
        return `
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/price.png" alt="Contact Person">
                ${item.price}</span>
        </div>
        `;
    }

    renderItems(data){
        var _self = this;
        $('.module_full_search .items').html('');
        $('.module_full_search .search').css('opacity', '1');
        $.each(data, function(index, item) {
            var html = `<div class="col l6 s12 m12">
                            <div class="card" data-lat="${item.lat}" data-lng="${item.lng}" data-url="${item.url}">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img src="${item.image}">
                                    <span class="card-title">${item.title}</span>
                                </div>
                                <div class="card-content">
                                    <div class="tabs_wrapper">
                                        <div class="row">
                                            <ul class="tabs">
                                                <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_item_${item.id}">About</a></li>
                                                <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_item_${item.id}">Info</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab_content" id="about_item_${item.id}">
                                        <p>${item.about}</p>
                                    </div>
                                    <div class="tab_content" id="info_item_${item.id}">

                                        ${_self.getItemBathrooms(item)}
                                        ${_self.getItemRooms(item)}
                                        ${_self.getItemFt(item)}
                                        ${_self.getItemPrice(item)}

                                    </div>
                                </div>
                                <div class="card-action">
                                    <a class="blue-text text-lighten-2" href="${item.url}">More</a>
                                    <a class="dropdown_shares" data-activates='dropdown_share_search_${item.id}'>
                                        <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
                                    </a>

                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown_share_search_${item.id}' class='dropdown-content'>
                                        ${item.share}
                                    </ul>
                                </div>
                            </div>
                        </div>`;
            $('.module_full_search .items').append(html);
            $('.dropdown_shares').dropdown({
                    belowOrigin: true, // Displays dropdown below the button
                }
            );
        });
        _self.onDataReceived();
    }

    onDataReceived(){
        this.loadMap();
        $('ul.tabs').tabs();
        $('body').trigger("sticky_kit:recalc");
    }

    updateSearch() {
        var _self = this
        var form = $('.module_full_search #search_form_form').serializeFormObject()
        var min_price = ($('.module_full_search #price_range_f_form .min').html()).replace(/\./g, "")
        var max_price = ($('.module_full_search #price_range_f_form .max').html()).replace(/\./g, "")
        form['min'] =  parseInt(min_price)
        form['max'] =  parseInt(max_price)

        var sendUrl  = ''
        var sendParams = {}

        $.each(form, function(k, v) {
            v = v || 'all';
            if(v !== 'all'){
                sendUrl +=  '/'+k+'-'+v
                sendParams[k] = v
            }
        })
        sendParams['action'] = "search_items";
        this.changeUrl('search', '/search'+sendUrl)
        $('.module_full_search .search').css('opacity', '.7');
        xhr(sendParams,function(response){
            _self.renderItems(response)
        }, function(){

        });

    }

    changeUrl(page, url) {
        if (typeof (history.pushState) != "undefined") {
            var obj = { Page: page, Url: url };
            history.pushState(obj, obj.Page, obj.Url);
        }
    }

    attachEvents() {
        var _self = this
        PubSub.subscribe('document.ready', function() {

            $('.module_full_search .available_from').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 16 // Creates a dropdown of 15 years to control year
            });

            var slider = $('.module_full_search #range-input');
            if(slider.length){
                slider = slider.get(0);
                var min =  parseInt( $('.module_full_search #range-input').attr('data-min').replace(/\./g, "") );
                var max =  parseInt( $('.module_full_search #range-input').attr('data-max').replace(/\./g, "") );

                var start =  parseInt( $('.module_full_search #range-input').attr('data-selected-min').replace(/\./g, "") ) || min;
                var end =  parseInt( $('.module_full_search #range-input').attr('data-selected-max').replace(/\./g, "") ) || max;

                noUiSlider.create(slider, {
                    start: [start, end],
                    connect: true,
                    step: 1,
                    range: {
                        'min': min,
                        'max': max
                    }
                });
                slider.noUiSlider.on('update', function(e){
                    $('.module_full_search #price_range_f_form .min').html(_self.toprice(parseInt(e[0])));
                    $('.module_full_search #price_range_f_form .max').html(_self.toprice(parseInt(e[1])));
                })
                slider.noUiSlider.on('change', function(){
                    _self.updateSearch();
                });
            }

            $('.module_full_search input,.module_full_search select').on('change', function(){
                _self.updateSearch();
            });

            _self.setMapHeight()
            _self.loadMap()

        });
    }
}

new FullSearch();