<div class="module_full_search row">

    <div class="map col l6 m6 s12">

    </div>

    <div class="search col l6 m6 s12">

        <div class="search_form_full_f_form row">

            <form id="search_form_form">

                <div class="row" style="margin-bottom: 0px ;">
                    <div class="input-field col l3 s6 m3">
                        <select name="actions">
                            <option value="all" selected>All Actions</option>
                            {foreach from=$search_params.actions item=action}
                                <option {if $action->slug == $query_data.actions} selected {/if} value="{$action->slug}">{$action->name|ucfirst}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="input-field col l3 s6 m3">
                        <select name="type">
                            <option value="all" selected>All Types</option>
                            {foreach from=$search_params.types item=type}
                                <option {if $type->slug == $query_data.type} selected {/if} value="{$type->slug}">{$type->name|ucfirst}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="input-field col l3 s6 m3">
                        <select name="city">
                            <option value="all" selected>All Cities</option>
                            {foreach from=$search_params.cities item=city}
                                <option {if $city->slug == $query_data.city} selected {/if} value="{$city->slug}">{$city->name|ucfirst}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="input-field col l3 s6 m3">
                        <select name="area">
                            <option value="all" selected>All Areas</option>
                            {foreach from=$search_params.areas item=area}
                                <option {if $area->slug == $query_data.area} selected {/if} value="{$area->slug}">{$area->name|ucfirst}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-top: 0px ;">
                    <div class="input-field col l3 s6 m3">
                        <select name="min_bedrooms">
                            <option value="0">Min. Bedrooms</option>
                            {foreach from=$search_params.bedrooms item=bedroom}
                                <option {if $bedroom == $query_data.min_bedrooms} selected {/if} value="{$bedroom}">{$bedroom}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="input-field col l3 s6 m3">
                        <label for="available_from" class="active">Available From</label>
                        <input name="available_from" value="{$query_data.available_from}" type="date" id="available_from" class="available_from">
                    </div>
                    <label id="price_range_f_form">Price range&nbsp;&nbsp;  from&nbsp;&nbsp;<span class="min">{$min}</span>&nbsp;&nbsp; to&nbsp;&nbsp;<span class="max">{$max}</span>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <div id="range-input" data-selected-min="{$query_data.min}" data-selected-max="{$query_data.max}"  data-min="{$search_params.min}" data-max="{$search_params.max}" class="input_range col l6 s12 m6" style="margin-top: 31px">
                    </div>
                </div>
            </form>
        </div>

        <div class="items">
            {foreach from=$items item=item}
                <div class="col l6 s12 m12">
                    <div class="card" data-lat="{$item.lat}" data-lng="{$item.lng}" data-url="{$item.url}" >
                        <div class="card-image waves-effect waves-block waves-light">
                            <img src="{$item.image}">
                            <span class="card-title">{$item.title}</span>
                        </div>
                        <div class="card-content">
                            <div class="tabs_wrapper">
                                <div class="row">
                                    <ul class="tabs">
                                        <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_item_{$item.id}">About</a></li>
                                        <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_item_{$item.id}">Info</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab_content" id="about_item_{$item.id}">
                                <p>{$item.about}</p>
                            </div>
                            <div class="tab_content" id="info_item_{$item.id}">
                                {if !empty({$item.bathrooms})}
                                    <div class="chip">
                                        <img src="/content/themes/titus/assets/images/icons/bathroom.png" alt="Contact Person">
                                        Bathooms<span class="new badge">{$item.bathrooms}</span>
                                    </div>
                                {/if}

                                {if !empty({$item.rooms})}
                                    <div class="chip">
                                        <img src="/content/themes/titus/assets/images/icons/bed.png" alt="Contact Person">
                                        Rooms<span class="new badge">{$item.rooms}</span>
                                    </div>
                                {/if}

                                {if !empty({$item.ft})}
                                    <div class="chip">
                                        <img src="/content/themes/titus/assets/images/icons/home.png" alt="Contact Person">
                                        {$item.ft} ft2</span>
                                    </div>
                                {/if}

                                {if !empty({$item.price})}
                                    <div class="chip">
                                        <img src="/content/themes/titus/assets/images/icons/price.png" alt="Contact Person">
                                        {$item.price}</span>
                                    </div>
                                {/if}

                            </div>
                        </div>
                        <div class="card-action">
                            <a class="blue-text text-lighten-2" href="{$item.url}">More</a>
                            <a class="dropdown_shares" data-activates='dropdown_share_search_{$item.id}'>
                                <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
                            </a>

                            <!-- Dropdown Structure -->
                            <ul id='dropdown_share_search_{$item.id}' class='dropdown-content'>
                                {$item.share}
                            </ul>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>

    </div>

</div>