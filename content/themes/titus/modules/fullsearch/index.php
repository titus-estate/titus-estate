<?php
namespace Modules;

class FullSearch extends \Modules\Search
{
    protected $query;

    protected function getSearchQueryData(){
        $url = esc_url($_SERVER["REQUEST_URI"]);
        $url = str_replace('/search/', '', $url);
        $searchData = explode('/', $url);
        $search = array();
        foreach ($searchData as $item) {
            $item = explode('-', $item);
            $key = $item[0];
            unset($item[0]);
            $value = implode('-', $item);
            $search[$key] = urldecode($value);
        }
        $this->query = $search;
        return($search);
    }

    public function getSearchItemsAjax(){
        $this->query = $_REQUEST;
        return $this->getItems();
    }

    protected function getItems() {
        $REQUEST = $this->query;
        $ret = array();
        $min = empty($REQUEST['min']) ? false : (int)$REQUEST['min'];
        $max = empty($REQUEST['max']) ? false : (int)$REQUEST['max'];
        $min_bedrooms = empty($REQUEST['min_bedrooms']) ? false : (int)$REQUEST['min_bedrooms'];
        $available_from = empty($REQUEST['available_from']) ? false : esc_html($REQUEST['available_from']);

        $actions = empty($REQUEST['actions']) ? false : esc_attr($REQUEST['actions']);
        $type = empty($REQUEST['type']) ? false : esc_attr($REQUEST['type']);
        $city = empty($REQUEST['city']) ? false : esc_attr($REQUEST['city']);
        $area = empty($REQUEST['area']) ? false : esc_attr($REQUEST['area']);
        $tax_query = array();
        $meta_query = array();


        if(!empty($actions)){
            $tax_query[] =   array(
                'taxonomy' => 'actions',
                'terms' => array($actions),
                'field' => 'slug',
                "operator" => "IN"
            );
        }

        if(!empty($city)){
            $tax_query[] =   array(
                'taxonomy' => 'city',
                'terms' => array($city),
                'field' => 'slug',
                "operator" => "IN"
            );
        }


        if(!empty($area)){
            $tax_query[] =   array(
                'taxonomy' => 'area',
                'terms' => array($area),
                'field' => 'slug',
                "operator" => "IN"
            );
        }


        if(!empty($type)){
            $tax_query[] =   array(
                'taxonomy' => 'type',
                'terms' => array($type),
                'field' => 'slug',
                "operator" => "IN"
            );
        }

        if(!empty($min) && !empty($max)){
            $meta_query[] =  array(
                'key'       => 'price',
                'value'     => array($min, $max),
                'compare'   => 'BETWEEN',
                'type' => 'NUMERIC'
            );
        }

        if(!empty($min_bedrooms)){
            $meta_query[] =  array(
                'key'       => 'bedrooms',
                'value'     => $min_bedrooms,
                'compare'   => '>=',
                'type' => 'NUMERIC'
            );
        }


        if(!empty($available_from)){
            $time = strtotime($available_from);
            $meta_query[] =  array(
                'key'       => 'available_from',
                'value'     => $time,
                'compare'   => '>=',
                'type' => 'NUMERIC'
            );
        }


        $search_posts = new \WP_Query(array(
            'post_type' => 'post',
            'showposts' => -1,
            'post_status'      => 'publish',
            'tax_query' => $tax_query,
            'meta_query'    => $meta_query,
            'orderby' => 'title',
            'order' => 'DESC'
        ));

        $posts = $search_posts->posts;
        foreach($posts as $post) {
            $post_data = get_fields($post->ID);
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '600x400' );
            $img = $thumb[0];
            $name = $post->post_title;
            $url = get_permalink($post);
            $excerpt = $post->post_excerpt;
            $ret[] = array(
                "image" => $img,
                "title" => $name,
                "id" => $post->ID,
                "about" => $excerpt,
                "url" => $url,
                "bathrooms" => $post_data["bathrooms"],
                "rooms" => $post_data["rooms"],
                "price" => number_format($post_data["price"]),
                "ft" => $post_data["property_size"],
                "lat" => !empty($post_data["address"]["lat"]) ? $post_data["address"]["lat"] : 0,
                "lng" => !empty($post_data["address"]["lng"]) ? $post_data["address"]["lng"] : 0,
                "share" => get_module("Share", array(
                    "post_id" => $post->ID
                ))
            );
        }
        return $ret;
    }

    protected function setData(){

        $search_params = $this->getSearchParams();
        $query_data = $this->getSearchQueryData();
        $items = $this->getItems();
        $this->data = array(
            "search_params" => $search_params,
            "query_data" => $query_data,
            "items" => $items
        );
    }


}