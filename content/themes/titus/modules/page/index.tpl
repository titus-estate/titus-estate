<div class="module_page">
    <h3 class="title">{$title}</h3>
    <div class="row content-area">
        {$content}
    </div>
</div>