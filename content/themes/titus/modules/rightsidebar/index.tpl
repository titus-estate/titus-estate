<div class="module_right_sidebar">
    {$banner}
    <div class="fb-page fb-widget-titus"
         data-href="{$facebook}"
         data-tabs="timeline"
         data-small-header="false"
         data-adapt-container-width="true"
         data-hide-cover="false"
         data-show-facepile="true"
         data-height="950"
    >
        <blockquote cite="{$facebook}"
                    class="fb-xfbml-parse-ignore"
        >
            <a href="{$facebook}">
                Titus Estate
            </a>
        </blockquote>
    </div>
    {$banner}
    {$contact_us_form}
</div>