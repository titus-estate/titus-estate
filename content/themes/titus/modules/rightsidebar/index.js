class RightSidebar  {

    constructor(){
        if($('.module_right_sidebar').length > 0){
            this.attachEvents()
        }
    }


    attachEvents() {
        var _self = this
        var height = window.innerHeight - 65

        $('.fb-widget-titus').attr('data-height', height)
        $('.content-area').css('min-height', $('.module_right_sidebar').height()+height)
        $('.fb-widget-titus').attr('data-width', $('.module_right_sidebar').width())

        PubSub.subscribe('window.resize', () => {
            var height = window.innerHeight - 65
            $('.fb-widget-titus').attr('data-height', height)
            $('.fb-widget-titus').attr('data-width', $('.module_right_sidebar').width())
            FB.XFBML.parse();
        });

        PubSub.subscribe('layoutChanged', () => {
            var height = window.innerHeight - 65
            $('.fb-widget-titus').attr('data-height', height)
            $('.fb-widget-titus').attr('data-width', $('.module_right_sidebar').width())
            FB.XFBML.parse();
        });

        PubSub.subscribe('document.ready', function() {
            $('.module_right_sidebar').stick_in_parent({
                spacer: false,
                offset_top: 65,
                recalc_every: 1,
                parent: $('.page-container-main').get(0)
            });
        });
    }
}

new RightSidebar();