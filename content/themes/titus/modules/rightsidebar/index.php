<?php
namespace Modules;

class RightSidebar extends  \Titus\Core\Module
{
    protected function setData(){
        global $post;
        $banner = "";
        $contact_us_form = "";
        if($post->post_name != 'contact-us'){
            $banner = get_module("Banner");
            $contact_us_form = get_module("ContactUsForm");
        }
        $this->data = array(
            "banner" => $banner,
            "facebook" => get_field('facebook_page_url', 'option'),
            "contact_us_form" => $contact_us_form
        );
    }
}