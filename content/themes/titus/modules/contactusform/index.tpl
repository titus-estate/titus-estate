<div class="module_contact_us_form">
    <h5>
        Contact Us
    </h5>
    <div class="row">
        <div class="contact_form col l12 m12 s12">
            <form  id="messageForm" method="post" >
                <div class="form-group form-group-label">
                    <div class="row">
                        <div class="col s12 m12 input-field">
                            <label class="floating-label" for="userName">Full Name</label>
                            <input class="data form-control input_text" name="userName" id="userName" type="text">
                        </div>
                    </div>
                </div>
                <div class="form-group form-group-label">
                    <div class="row">
                        <div class="col s12 m12 input-field">
                            <label class="floating-label" for="message_email">Email Address</label>
                            <input class="data form-control input_email" name="message_email" id="message_email" type="email" >
                        </div>
                    </div>
                </div>
                <div class="form-group form-group-label">
                    <div class="row">
                        <div class="col s12 m12 input-field">
                            <label class="floating-label" for="message_text">Your Message</label>
                            <textarea class="data form-control input_textarea materialize-textarea textarea" name="message_text" id="message_text" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group form-group-label">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <i class="step_submit btn btn-primary right waves-effect waves-light waves-input-wrapper" style=""><input class="waves-button-input" type="button" data-step="step_messageForm_step_1" value="Send"></i>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>