<?php
namespace Modules;

class ContactUsForm extends  \Titus\Core\Module
{
    public function add() {

        $userName = ( !empty($_REQUEST['user_name'])) ? sanitize_text_field($_REQUEST['user_name']) : false;
        $message_email = ( !empty($_REQUEST['email'])) ? sanitize_email($_REQUEST['email']) : false;
        $message_text = ( !empty($_REQUEST['message'])) ? sanitize_text_field($_REQUEST['message']) : false;

        if(!$userName || !$message_email || !$message_text){
            echo 'false';
            return 0;
        }
        $errors = array();
        if(strlen($userName) < 6){
            $errors[] = 'userName';
        }if(strlen($message_text) < 10){
            $errors[] = 'message_text';
        }if(!filter_var($message_email, FILTER_VALIDATE_EMAIL)){
            $errors[] = 'message_email';
        }
        if(!empty($errors)){
            echo json_encode($errors);
            return 0;
        }else{
            $subject = "Titus Estate: Message from ". $userName. "($message_email)";
            wp_mail( get_field('email', 'options'), $subject, $message_text );
            echo 'true';
        }
    }
}