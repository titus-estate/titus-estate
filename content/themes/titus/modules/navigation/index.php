<?php
namespace Modules;

class Navigation extends \Titus\Core\Module
{

    protected function setData()
    {
        $menu = wp_get_nav_menu_items('Titus');
        $this->data = [
            "menu_items" => $menu
        ];
    }

}