<div class="module_navigation">
    <div class="logo"></div>
    <div class="navigation">
        <nav>
            <div class="nav-wrapper blue lighten-2">
                <a href="#" data-activates="mobile-demo" class="button-collapse right waves-effect waves-light"><i class="fa fa-bars"></i></a>
                <ul class="center-align hide-on-med-and-down">
                    {foreach from=$menu_items item=menu_item}
                        <li class="waves-effect waves-light"><a href="{$menu_item->url}">{$menu_item->title}</a></li>
                    {/foreach}

                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <div class="mobile-logo"></div>
                    {foreach from=$menu_items item=menu_item}
                        <li><a href="{$menu_item->url}">{$menu_item->title}</a></li>
                    {/foreach}
                </ul>
            </div>
        </nav>
    </div>
</div>