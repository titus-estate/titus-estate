class Navigation  {

    constructor(){
        this.attachEvents()
    }

    stickHeader(mq){

        if(mq == 'mobile' && !$('body').hasClass('header_sticked')){
            $('body').addClass('header_sticked')
        }else if($(window).scrollTop() >= 40 && !$('body').hasClass('header_sticked')){
            $('body').addClass('header_sticked')
        }else if($(window).scrollTop() < 40 && $('body').hasClass('header_sticked')){
            $('body').removeClass('header_sticked')
        }
    }

    attachEvents() {
        var _self = this

        PubSub.subscribe('document.ready', () => {
            $(".button-collapse").sideNav()
            _self.stickHeader(window.mq)
        })

        PubSub.subscribe('window.scroll', () => {

            if(window.mq == "mobile") return

            _self.stickHeader(window.mq)
        })

        PubSub.subscribe('layoutChanged', (event, mq) => {
            _self.stickHeader(mq)
        })
    }
}

new Navigation();

