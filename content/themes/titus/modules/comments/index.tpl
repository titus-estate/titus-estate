<div class="module_comments">
    <div class="container">
        <div class="row">
            <div class="col l12 m12 s12">
                <h5>Client Testimonials</h5>
                {foreach from=$comments  item=comment}
                    <div class="comment col l6 m12 s12">
                        <div class="testimonial-container">
                            <div class="testimonial-image"></div>
                            <div class="testimonial-text">
                                {$comment->comment_content}
                            </div>
                            <div class="testimonial-author-line">
                                <span class="testimonial-author">{$comment->comment_author}</span>, ({$comment->comment_author_email})
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>