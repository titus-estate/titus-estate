<?php
namespace Modules;

class Comments extends  \Titus\Core\Module
{
    public function setData() {
        $args = array(
            'number' => '2',
        );
        $comments = get_comments($args);
        $this->data = array("comments" => $comments);
    }

}