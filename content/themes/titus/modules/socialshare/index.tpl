<div class="module_socialShare">
        {if !empty($facebook)}
                <a href="{$facebook}"><i class="fa fa-facebook"></i>&nbsp;&nbsp;</a>
        {/if}

        {if !empty($twitter)}
                <a href="{$twitter}"><i class="fa fa-twitter"></i>&nbsp;&nbsp;</a>
        {/if}

        {if !empty($google_plus)}
                <a href="{$google_plus}"><i class="fa fa-google-plus"></i>&nbsp;&nbsp;</a>
        {/if}

        {if !empty($instagram)}
                <a href="{$instagram}"><i class="fa fa-instagram"></i>&nbsp;&nbsp;</a>
        {/if}

        {if !empty($pinterest)}
                <a href="{$pinterest}"><i class="fa fa-pinterest"></i>&nbsp;&nbsp;</a>
        {/if}

</div>
</div>