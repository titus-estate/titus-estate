<?php
namespace Modules;

class SocialShare extends \Titus\Core\Module
{
    protected function setData(){

        $this->data = array(
            "facebook" => get_field('facebook_page_url', 'option'),
            "twitter" => get_field('twitter_page_url', 'option'),
            "google_plus" => get_field('google_plus_page_url', 'option'),
            "instagram" => get_field('instagram_page_url', 'option'),
            "pinterest" => get_field('pinterest_page_url', 'option'),
        );
    }
}