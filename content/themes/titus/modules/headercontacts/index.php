<?php
namespace Modules;

class HeaderContacts extends  \Titus\Core\Module
{
    protected function setData(){

        $this->data = array(
            "phone" => get_field('phone', 'option'),
            "email" => get_field('email', 'option')
        );
    }
}