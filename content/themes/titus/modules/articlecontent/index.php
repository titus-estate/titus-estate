<?php
namespace Modules;
class ArticleContent extends \Titus\Core\Module
{

    protected function createString($tags){
        $ret = "";
        foreach ($tags as $tag){
            $ret .= ucfirst($tag->name).", ";
        }
        $ret = trim($ret, ', ');
        return $ret;
    }

    protected function setData(){
        global $post;

        $content = $post->post_content;
        $content = apply_filters('the_content', $content);
        $post_data =get_fields($post->ID);
        $currentTime = date('d M Y ', $post_data['available_from']);
        $post_data['available_from'] = $currentTime;
        $post_data['contact_info']['display_name'] = ucfirst($post_data['contact_info']['display_name']);
        $features = wp_get_object_terms( $post->ID, 'features');
        $actions = wp_get_object_terms( $post->ID, 'actions');
        $tags = wp_get_object_terms( $post->ID, 'post_tag');
        $city = wp_get_object_terms( $post->ID, 'city');
        $area = wp_get_object_terms( $post->ID, 'area');
        $country = wp_get_object_terms( $post->ID, 'country');
        $state = wp_get_object_terms( $post->ID, 'state');
        $types = wp_get_object_terms( $post->ID, 'type');

        $this->data = array(
            "content" => $content,
            "post_data" => $post_data,
            "title" => $post->post_title,
            "share" => get_module("Share"),
            "post" => $post,
            "features" => $features,
            "types" => $this->createString($types),
            "actions" => $this->createString($actions),
            "tags" => $this->createString($tags),
            "city" => $this->createString($city),
            "area" => $this->createString($area),
            "country" => $this->createString($country),
            "state" => $this->createString($state),
            "agent" => get_module("Agent"),
        );
    }
}