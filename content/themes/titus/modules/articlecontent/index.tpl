<div class="module_articlecontent">
    <h3 class="title">{$title}</h3>
    {if !empty($post_data.bathrooms)}
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/bathroom.png" alt="Contact Person">
            Bathooms<span class="new badge">{$post_data.bathrooms}</span>
        </div>
    {/if}

    {if !empty($post_data.rooms)}
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/bed.png" alt="Contact Person">
            Rooms<span class="new badge">{$post_data.rooms}</span>
        </div>
    {/if}

    {if !empty($post_data.property_size)}
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/home.png" alt="Contact Person">
            {$post_data.property_size} ft2</span>
        </div>
    {/if}

    {if !empty($post_data.price)}
        <div class="chip">
            <img src="/content/themes/titus/assets/images/icons/price.png" alt="Contact Person">
            {$post_data.price}</span>
        </div>
    {/if}

    <br>
    <br>
    <br>

    <div class="features">
        <div class="title_panel">
            <h4>
                General Information
            </h4>
        </div>
        <div class="row content_f">
            <div class="col l6 m6 s12">Property Id : #{$post->ID}</div>

            {if !empty($types)}
                <div class="col l6 m6 s12">Type: {$types}</div>
            {/if}

            {if !empty($actions)}
                <div class="col l6 m6 s12">Action: {$actions}</div>
            {/if}

            {if !empty($post_data.price)}
                <div class="col l6 m6 s12">Price: $ {$post_data.price}</div>
            {/if}

            {if !empty($post_data.property_size)}
                <div class="col l6 m6 s12">Property Size: {$post_data.property_size} ft2</div>
            {/if}

            {if !empty($post_data.property_size)}
                <div class="col l6 m6 s12">Property Lot Size: {$post_data.property_size} ft2</div>
            {/if}

            {if !empty($post_data.rooms)}
                <div class="col l6 m6 s12">Rooms: {$post_data.rooms}</div>
            {/if}

            {if !empty($post_data.bedrooms)}
                <div class="col l6 m6 s12">Bedrooms: {$post_data.bedrooms}</div>
            {/if}

            {if !empty($post_data.bathrooms)}
                <div class="col l6 m6 s12">Bathrooms: {$post_data.bathrooms}</div>
            {/if}

            {if !empty($post_data.available_from)}
                <div class="col l6 m6 s12">Available From: {$post_data.available_from}</div>
            {/if}

            {if !empty($post_data.zip)}
                <div class="col l6 m6 s12">Zip: {$post_data.zip}</div>
            {/if}

            <div class="col l6 m6 s12">Agent: <a class="link" href="/agents/{$post_data.contact_info.nickname}">{$post_data.contact_info.display_name}</a></div>
        </div>
    </div>

    <div class="row content-area">
        {$content}
    </div>

    {if !empty($post_data.documents)}
        <div class="features">
            <div class="title_panel">
                <h4>
                    Documents
                </h4>
            </div>
            <div class="row content_f">
                {foreach from=$post_data.documents  item=document}
                    <div class="col l12 m12 s12"><a href="{$document.file.url}" ><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp; {$document.file.title}</a></div>
                    <hr>
                {/foreach}
            </div>
        </div>
    {/if}

    {if !empty($features)}
        <div class="features">
            <div class="title_panel">
                <h4>
                    Amenities and Features
                </h4>
            </div>
            <div class="row content_f">
                {foreach from=$features  item=feature}
                    <div class="col l4 m4 s12">
                        <i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;{$feature->name}
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}

    {if !empty($post_data.details)}
        <div class="features">
            <div class="title_panel">
                <h4>
                    Other Information
                </h4>
            </div>
            <div class="row content_f">
                {foreach from=$post_data.details  item=detail}
                    <div class="col l6 m6 s12">{$detail.key} : {$detail.value}</div>
                {/foreach}
            </div>
        </div>
    {/if}



    {if !empty($post_data.address)}
        <div class="features">
            <div class="title_panel">
                <h4>
                    Address
                </h4>
            </div>
            <div class="row content_f">
                {if !empty($city)}
                    <div class="col l6 m6 s12">City: {$city}</div>
                {/if}

                {if !empty($area)}
                    <div class="col l6 m6 s12">Area: {$area}</div>
                {/if}

                {if !empty($country)}
                    <div class="col l6 m6 s12">Country: {$country}</div>
                {/if}

                {if !empty($state)}
                    <div class="col l6 m6 s12">State: {$state}</div>
                {/if}

                <div class="col l12 s12 m12 article_map" data-lat="{$post_data.address.lat}" data-lng="{$post_data.address.lng}" data-address="{$post_data.address.address}">

                </div>
            </div>
        </div>
    {/if}




    <br>
    <br>
    {$share}
    <br>
    <br>

    {$agent}
    <br>
    <br>
</div>