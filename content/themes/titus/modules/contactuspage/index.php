<?php

namespace Modules;

class ContactUsPage extends \Titus\Core\Module
{
    protected function setData(){
        global $post;
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
        $img = $thumb[0];
        $content = $post->post_content;
        $content = apply_filters('the_content', $content);
        $this->data = array(
            "address" => get_field('address', 'options')['address'],
            "phone" => get_field('phone', 'options'),
            "email" => get_field('email', 'options'),
            "skype" => get_field('skype', 'options'),
            "featured_image" => $img,
            "content" => $content
        );
    }
}