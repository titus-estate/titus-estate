<div class="module_contact_us_page">
    <h3 class="title">Contact Us</h3>
    <div class="info">
        <div class="col l3 m5 s12 left_image">
            <img src="{$featured_image}">
        </div>
        <div class="col l9 m7 s12">
            <h5>Titus Estate</h5>
            <hr/>
            <div class="phone"><i class="fa fa-phone"></i>&nbsp;&nbsp; {$phone}&nbsp;&nbsp;&nbsp;</div>
            <div class="mail"><i class="fa fa-envelope"></i>&nbsp;&nbsp;{$email}</div>
            <div class="skype"><i class="fa fa-skype"></i>&nbsp;&nbsp;&nbsp;{$skype}</div>
            <div class="address"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;&nbsp;{$address}</div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="row content-area">
        {$content}
    </div>
</div>