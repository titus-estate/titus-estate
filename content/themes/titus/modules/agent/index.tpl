<div class="module-agent">
    <div class="info">
        <div class="row">
            <div class="col l2 m3 s12 left_image">
                {$agent.user_avatar}
            </div>
            <div class="col l10 m9 s12">
                <h5>{$agent.user_firstname} {$agent.user_lastname}</h5>
                <hr/>
                <div class="phone"><i class="fa fa-phone"></i>&nbsp;&nbsp; {$agent.phone}&nbsp;&nbsp;&nbsp;</div>
                <div class="mail"><i class="fa fa-envelope"></i>&nbsp;&nbsp;{$agent.user_email}</div>
                <div class="skype"><i class="fa fa-skype"></i>&nbsp;&nbsp;&nbsp;{$agent.skype}</div>
            </div>
        </div>
    </div>

</div>