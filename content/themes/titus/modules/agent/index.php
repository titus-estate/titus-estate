<?php

namespace Modules;

class Agent extends \Titus\Core\Module
{
    protected function setData(){
        global $post;
        $contact_info = get_field('contact_info', $post->ID);
        $contact_info['phone'] = get_field('phone', 'user_'.$contact_info['ID']);
        $contact_info['skype'] = get_field('skype', 'user_'.$contact_info['ID']);
        $this->data = array(
            "address" => get_field('address', 'options')['address'],
            "phone" => get_field('phone', 'options'),
            "email" => get_field('email', 'options'),
            "skype" => get_field('skype', 'options'),
            "agent" => $contact_info
        );
    }
}