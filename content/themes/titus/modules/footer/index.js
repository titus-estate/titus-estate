var Map = require("utils/map")

class Footer  {

    constructor(){
        if($('.module_footer').length > 0){

            this.map = false;
            this.loadMap()

        }
    }

    loadMap(){

        var coordinates = [
            {
                'latitude' : $('.module_footer .footer_map').attr('data-lat'),
                'longitude' : $('.module_footer .footer_map').attr('data-lng'),
                'icon' : "/content/themes/titus/assets/images/marker_pin.png"
            }
        ];
        if(!this.map) {
            var _self = this;
            this.map =  new Map($('.module_footer .footer_map').get(0), coordinates)
            this.map.init()
            PubSub.subscribe('onGoogleMapApiReady', function() {
                _self.map.zoomTo(15)
            });


        }else {
            this.map.updateMarkers(coordinates);
        }

    }
}

new Footer();