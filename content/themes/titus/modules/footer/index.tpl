<div class="module_footer">
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l4 s12">
                    <h5 class="white-text">Titus Realty</h5>
                    <p class="grey-text text-lighten-4">
                        Titus Realty, Inc. has been servicing the local community for over 15 years by providing residential and commercial real estate services.
                    </p>
                </div>
                <div class="col l2  s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        {foreach from=$menu_items item=menu_item}
                            <li><a class="grey-text text-lighten-3" href="{$menu_item->url}">{$menu_item->title}</a></li>
                        {/foreach}
                    </ul>
                </div>
                <div class="col l6  s12 footer_map" data-lat="{$address.lat}" data-lng="{$address.lng}" data-address="{$address.address}">

                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2016 Titus Realty, Inc.
                <a class="grey-text text-lighten-4 right" href="#!">Titus Realty</a>
            </div>
        </div>
    </footer>
</div>