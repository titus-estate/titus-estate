<?php

namespace Modules;

class Footer extends \Titus\Core\Module
{
    protected function setData(){
        $this->data = array(
            "address" => get_field('address', 'options'),
            "menu_items" => wp_get_nav_menu_items('Titus')
        );
    }
}