var Map = require("utils/map")

class ContactUsMap  {

    constructor(){
        if($('.module_contact_us_map').length > 0){
            this.map = false;
            this.loadMap()
        }
    }

    loadMap(){

        var _self = this
        PubSub.subscribe('window.resize', () => {
            _self.map.fitMarkers()
            _self.map.zoomTo(10)
        });

        PubSub.subscribe('layoutChanged', () => {
            _self.map.fitMarkers()
            _self.map.zoomTo(10)
        });

        var coordinates = [
            {
                'latitude' : $('.module_contact_us_map .map').attr('data-lat'),
                'longitude' : $('.module_contact_us_map .map').attr('data-lng'),
                'icon' : "/content/themes/titus/assets/images/marker_pin.png"
            }
        ];
        if(!this.map) {
            var _self = this;
            this.map =  new Map($('.module_contact_us_map .map').get(0), coordinates)
            this.map.init()
            PubSub.subscribe('onGoogleMapApiReady', function() {
                _self.map.zoomTo(10)
            });


        }else {
            this.map.updateMarkers(coordinates);
        }

    }
}

new ContactUsMap();