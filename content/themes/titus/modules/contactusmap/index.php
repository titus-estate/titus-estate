<?php

namespace Modules;

class ContactUsMap extends \Titus\Core\Module
{
    protected function setData(){

        $this->data = array(
            "address" => get_field('address', 'options')
        );
    }
}