<div class="row">
    <div class="module_search col l10 m12 s12 push-l1">
        <div class="top blue lighten-2">Advanced Search</div>
        <form id="search_form_form">
            <div class="search_form">
                <div class="row">
                    <div class="select_form col l10 m10 s12">
                        <div class="row" style="margin-bottom: 0px ;">
                            <div class="input-field col l3 s6 m3">
                                <select name="actions">
                                    <option value="all" selected>All Actions</option>
                                    {foreach from=$actions item=action}
                                        <option value="{$action->slug}">{$action->name|ucfirst}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="input-field col l3 s6 m3">
                                <select name="type">
                                    <option value="all" selected>All Types</option>
                                    {foreach from=$types item=type}
                                        <option value="{$type->slug}">{$type->name|ucfirst}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="input-field col l3 s6 m3">
                                <select name="city">
                                    <option value="all" selected>All Cities</option>
                                    {foreach from=$cities item=city}
                                        <option value="{$city->slug}">{$city->name|ucfirst}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="input-field col l3 s6 m3">
                                <select name="area">
                                    <option value="all" selected>All Areas</option>
                                    {foreach from=$areas item=area}
                                        <option value="{$area->slug}">{$area->name|ucfirst}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px ;">
                            <div class="input-field col l3 s6 m3">
                                <select name="min_bedrooms">
                                    <option value="0" selected>Min. Bedrooms</option>
                                    {foreach from=$bedrooms item=bedroom}
                                        <option value="{$bedroom}">{$bedroom}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="input-field col l3 s6 m3">
                                <label for="available_from" class="active">Available From</label>
                                <input name="available_from" type="date" id="available_from" class="available_from">
                            </div>
                            <label id="price_range_f_form">Price range&nbsp;&nbsp;  from&nbsp;&nbsp;<span class="min">{$min}</span>&nbsp;&nbsp; to&nbsp;&nbsp;<span class="max">{$max}</span>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <div id="range-input"  data-min="{$min}" data-max="{$max}" class="input_range col l6 s12 m6" style="margin-top: 31px">
                            </div>
                        </div>
                    </div>
                    <div class="submit_form col l2 m2 s12"><i class="fa fa-search"></i></div>
                </div>
            </div>
        </form>
    </div>
</div>