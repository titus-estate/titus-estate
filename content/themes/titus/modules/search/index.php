<?php
namespace Modules;

class Search extends \Titus\Core\Module
{

    protected $publishedIds = false;

    protected function getPublishedPostIds(){
        $post_ids = get_posts(array(
            'post_type' => 'post',
            'showposts' => -1,
            'post_status'      => 'publish',
            'fields'        => 'ids', // Only get post IDs
        ));
        $post_ids = implode(',', $post_ids);
        return $post_ids;
    }

    protected function getPriceRange(){
        $ret = array(
            "min" => 1800,
            "min" => 350000
        );
        global $wpdb;
        $sql="SELECT 
                max(cast(meta_value as unsigned)) AS max, 
                min(cast(meta_value as unsigned)) AS min
                FROM wp_postmeta WHERE meta_key='price' AND post_id IN ({$this->publishedIds})";
        $range = $wpdb->get_results($sql);
        if(!empty($range)){
            $ret["min"] = $range[0]->min;
            $ret["max"] = $range[0]->max;
        }
        return $ret;
    }

    protected function getMinPrice(){

    }

    protected function getSearchParams(){

        $this->publishedIds = $this->getPublishedPostIds();
        $priceRange  = $this->getPriceRange();
        $args = array(
            'type'                     => 'post',
            'child_of'                 => 0,
            'parent'                   => '',
            'orderby'                  => 'name',
            'order'                    => 'ASC',
            'hide_empty'               => false,
            'hierarchical'             => 1,
            'exclude'                  => '',
            'include'                  => '',
            'number'                   => '',
            'taxonomy'                 => 'category',
            'pad_counts'               => false
        );
        $categories  = get_categories($args);

        $args['taxonomy'] = 'actions';
        $actions = get_categories($args);


        $args['taxonomy'] = 'type';
        $types = get_categories($args);

        $args['taxonomy'] = 'city';
        $cities = get_categories($args);

        $args['taxonomy'] = 'area';
        $areas = get_categories($args);



        $data = array(
            "actions" => $actions,
            "types" => $types,
            "cities" => $cities,
            "areas" => $areas,
            "bedrooms" => array(1, 2, 3, 4, 5, 6),
            "max" => $priceRange["max"],
            "min" => $priceRange["min"]
        );

        return $data;

    }

    protected function setData(){

        $this->data = $this->getSearchParams();
    }

}