class Search  {

    constructor(){
        this.attachEvents()
    }

    toprice(x){
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return parts.join(".");
    }

    attachEvents() {
        var _self = this
        PubSub.subscribe('document.ready', function() {

            $('.module_search .available_from').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 16 // Creates a dropdown of 15 years to control year
            });

            var slider = $('.module_search #range-input');
            if(slider.length){
                slider = slider.get(0);
                var min =  parseInt( $('.module_search #range-input').attr('data-min').replace(/\./g, "") );
                var max =  parseInt( $('.module_search #range-input').attr('data-max').replace(/\./g, "") );
                noUiSlider.create(slider, {
                    start: [min + 50000, max -50000],
                    connect: true,
                    step: 1,
                    range: {
                        'min': min,
                        'max': max
                    }
                });
                slider.noUiSlider.on('update', function(e){
                    $('.module_search #price_range_f_form .min').html(_self.toprice(parseInt(e[0])));
                    $('.module_search #price_range_f_form .max').html(_self.toprice(parseInt(e[1])));
                });
            }


            $('.module_search .search_form .submit_form').on('click', function(){
                var form = $('.module_search #search_form_form').serializeFormObject();
                var min_price = $('.module_search #price_range_f_form .min').html().replace(/\./g, "");
                var max_price = $('.module_search #price_range_f_form .max').html().replace(/\./g, "");
                form['min'] = (parseInt(min_price) == min) ? 'all' : parseInt(min_price);
                form['max'] = (parseInt(max_price) == max) ? 'all' : parseInt(max_price);
                var sendUrl  = '/search';
                $.each(form, function(k, v) {
                    v = v || 'all';
                    if(v !== 'all'){
                        sendUrl +=  '/'+k+'-'+v;
                    }
                });

                window.location.href = sendUrl;

            });

        });
    }
}

new Search();








