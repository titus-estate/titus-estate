<div class="module_slide">
    <div class="slider fullscreen">
        {if $showInfo == false}
            <span class="slide-button slide-prev">
                <i class="fa fa-angle-left"></i>
            </span>
            <span class="slide-button slide-next">
                <i class="fa fa-angle-right"></i>
            </span>
        {/if}
        <ul class="slides">
            {foreach from=$slider item=sliderItem}
                <li>
                    <a href="{$sliderItem.url}">
                        <img src="{$sliderItem.image}">
                        {if $showInfo == true}
                            <div class="caption slider-card {$sliderItem.captionClass}-align">
                                <div class="slider-content">
                                    <h3>
                                        <a href="{$sliderItem.url}">
                                            {$sliderItem.title|truncate:30:"...":true}
                                        </a>
                                    </h3>
                                    <div class="slider-card-content">
                                        <div>
                                            {$sliderItem.subTitle|truncate:100:"...":true}
                                        </div>
                                        <a class="slider-card-read-more" href="{$sliderItem.url}" >
                                            Read more
                                            <i class="fa fa-angle-right">

                                            </i>
                                        </a>
                                        <div style="clear:both;"></div>
                                    </div>
                                    <div class="theme-slider-price">
                                        <span class="price_label price_label_before"></span> $ {$sliderItem.price}
                                    </div>
                                    <a class=" slide-next carousel-control-theme-next" href="#" data-slide="next"><i class="fa fa-angle-right"></i></a>
                                    <a class=" slide-prev carousel-control-theme-prev" href="#" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                </div>
                            </div>
                        {/if}
                    </a>
                </li>
            {/foreach}
        </ul>
    </div>
</div>