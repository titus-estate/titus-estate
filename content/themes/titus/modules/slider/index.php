<?php
namespace Modules;

class Slider extends \Titus\Core\Module
{
    protected function setData(){

        if(!empty($this->args)){
            $slider_images = array();
            foreach ($this->args['data'] as $image){
                $slider_images[] = array(
                    "image" =>$image,
                    "title" => "",
                    "subTitle" => "",
                    "url" => "",
                    "captionClass" => "right"
                );
            }
            $this->data = array(
                "slider" => $slider_images,
                "showInfo" => false
            );
            return;
        }

        $featured_posts =  get_field('featured_posts', 'option');
        $slider = array();
        foreach($featured_posts as $featured_post){
            $post = get_post($featured_post);
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
            $img = $thumb[0];
            $name = $post->post_title;
            $url = get_permalink($post);
            $excerpt = $post->post_excerpt;
            $slider[] = array(
                "image" =>$img,
                "title" => $name,
                "subTitle" => $excerpt,
                "url" => $url,
                "price" => number_format(get_field("price", $post->ID)),
                "captionClass" => "right"
            );

        }
        $this->data = array(
            "slider" => $slider,
            "showInfo" => true
        );

    }
}