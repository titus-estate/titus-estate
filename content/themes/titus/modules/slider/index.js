PubSub.subscribe('document.ready', function(){
    $('.slider').slider(
        {
            full_width: true,
            indicators: false
        }
    );

    $('.module_slide').hover(function(){
        $('.slider').slider('pause');
    }, function(){
        $('.slider').slider('start');
    });

    $('.slide-next').click(function(){
        $('.slider').slider('next');
    });

    $('.slide-prev').click(function() {
        $('.slider').slider('prev');
    });

});