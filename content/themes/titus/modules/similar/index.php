<?php
namespace Modules;
class Similar extends \Titus\Core\Module
{

    protected function getSimilar(){
        global $post;
        $show_similars = get_field('show_similar_listings', $post->ID);
        if(!$show_similars){
            return array();
        }
        $posts = get_field('similar_listing', $post->ID);
        $ret = array();
        foreach($posts as $post) {
            $post = get_post($post);
            $post_data = get_fields($post->ID);
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '600x400' );
            $img = $thumb[0];
            $name = $post->post_title;
            $url = get_permalink($post);
            $excerpt = $post->post_excerpt;
            $ret[] = array(
                "image" => $img,
                "title" => $name,
                "id" => $post->ID,
                "about" => $excerpt,
                "url" => $url,
                "bathrooms" => $post_data["bathrooms"],
                "rooms" => $post_data["rooms"],
                "price" => number_format($post_data["price"]),
                "ft" => $post_data["property_size"],
                "share" => get_module("Share", array(
                    "post_id" => $post->ID
                ))
            );
        }
        return $ret;
    }

    protected function setData(){

        $similar = $this->getSimilar();
        $this->data = array(
            "similar" => $similar,
            "title" => !empty($similar) ? "Similar listings" : "",
        );
    }

}