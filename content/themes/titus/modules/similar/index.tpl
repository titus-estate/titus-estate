<div class="module_similar">
    <div class="row">
        <div class="title"><h5>{$title}</h5></div>
        {foreach from=$similar  item=sale}
            <div class="col l4 s12 m6">

                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img src="{$sale.image}">
                        <span class="card-title">{$sale.title}</span>
                    </div>
                    <div class="card-content">
                        <div class="tabs_wrapper">
                            <div class="row">
                                <ul class="tabs" >
                                    <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_sale_{$sale.id}">About</a></li>
                                    <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_sale_{$sale.id}">Info</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab_content" id="about_sale_{$sale.id}">
                            <p>{$sale.about}</p>
                        </div>
                        <div class="tab_content" id="info_sale_{$sale.id}">

                            {if !empty({$sale.bathrooms})}
                                <div class="chip">
                                    <img src="/content/themes/titus/assets/images/icons/bathroom.png" alt="Contact Person">
                                    Bathooms<span class="new badge">{$sale.bathrooms}</span>
                                </div>
                            {/if}

                            {if !empty({$sale.rooms})}
                                <div class="chip">
                                    <img src="/content/themes/titus/assets/images/icons/bed.png" alt="Contact Person">
                                    Rooms<span class="new badge">{$sale.rooms}</span>
                                </div>
                            {/if}

                            {if !empty({$sale.ft})}
                                <div class="chip">
                                    <img src="/content/themes/titus/assets/images/icons/home.png" alt="Contact Person">
                                    {$sale.ft} ft2</span>
                                </div>
                            {/if}

                            {if !empty({$sale.price})}
                                <div class="chip">
                                    <img src="/content/themes/titus/assets/images/icons/price.png" alt="Contact Person">
                                    {$sale.price}</span>
                                </div>
                            {/if}

                        </div>
                    </div>
                    <div class="card-action">
                        <a class="blue-text text-lighten-2" href="{$sale.url}">More</a>
                        <a class="dropdown_shares" data-activates='dropdown_share_{$sale.id}'>
                            <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
                        </a>

                        <!-- Dropdown Structure -->
                        <ul id='dropdown_share_{$sale.id}' class='dropdown-content'>
                            {$sale.share}
                        </ul>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>