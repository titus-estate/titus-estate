<div class="module_testimonials">
    <div class="row">
        <div class="contact_form col l4 m6 s12">
            <div class="container">
                <form  id="messageForm" method="post" >
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col s12 m12 input-field">
                                <label class="floating-label" for="userName">Full Name</label>
                                <input class="data form-control input_text" name="userName" id="userName" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col s12 m12 input-field">
                                <label class="floating-label" for="message_email">Email Address</label>
                                <input class="data form-control input_email" name="message_email" id="message_email" type="email" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col s12 m12 input-field">
                                <label class="floating-label" for="message_text">Your Message</label>
                                <textarea class="data form-control input_textarea materialize-textarea textarea" name="message_text" id="message_text" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-md-12">
                                <i class="step_submit btn btn-primary right waves-effect waves-light waves-input-wrapper" style=""><input class="waves-button-input" type="button" data-step="step_messageForm_step_1" value="Send"></i>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="about col l8 m6 s12 ">
            <table>
                <tr>
                    <td>
                        <h3>Valuable Real Estate Tools & Info!</h3>
                        <img src="/content/themes/titus/assets/images/about_as1.jpg"/>
                        <img src="/content/themes/titus/assets/images/about_as2.jpg"/>
                        <img src="/content/themes/titus/assets/images/about_as3.jpg"/>
                        <p>
                            The WP Estate team did an outstanding job helping me buy my first home. The high level of service and dedication to seeing things done the right way is what I look for in an agent. The WP Estate team delivered on that expectation and I would highly recommend them to anyone who is in the market to buy a home.
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>