import Validate from 'utils/validate'
import xhr from 'utils/xhr'

class Testimonials  {

    constructor(){
        this.attachEvents()
    }

    attachEvents() {
        PubSub.subscribe('document.ready', function(){

            $('.module_testimonials .contact_form .step_submit').click(function() {

                var validate = new Validate();
                var data = $('.module_testimonials #messageForm').serializeFormObject();
                var email = data['message_email'];
                var name = data['userName'];
                var message = data['message_text'];
                var $el = $('.module_testimonials .contact_form');

                var is_email = validate.hooks.valid_email(email);
                var is_name = validate.hooks.min_length(name, 6);
                var is_message = validate.hooks.min_length(message, 6);
                $el.find('.error').removeClass('error');
                if (!is_email) {
                    $el.find('#message_email').addClass('error');
                }
                if (!is_name) {
                    $el.find('#userName').addClass('error');
                }
                if (!is_message) {
                    $el.find('#message_text').addClass('error');
                }
                if (is_email && is_name && is_message) {
                    $('.module_testimonials .contact_form .step_submit input').val('.......');
                    xhr({
                        action: 'testimonials',
                        email: email,
                        message: message,
                        user_name: name

                    }, function (response) {
                        $('.module_testimonials .contact_form .step_submit input').val('SEND');
                        if (response === true) {
                            $el.find('#message_email').val('');
                            $el.find('#userName').val('');
                            $el.find('#message_text').val('');
                            Materialize.toast('Success!', 4000);
                        } else if (response == 'error') {
                            Materialize.toast('Server Error!', 4000);
                        } else if(response === false){
                            $el.find('#message_email').addClass('error');
                            $el.find('#userName').addClass('error');
                            $el.find('#message_text').addClass('error');
                        }else {
                            response.map(function(val){
                                $el.find('#'+val).addClass('error');
                            });
                        }
                    }, function () {
                        Materialize.toast('Server Error!', 4000);
                        $('.module_testimonials .contact_form .step_submit input').val('SEND');
                    });
                }

            });

        });
    }
}

new Testimonials();













