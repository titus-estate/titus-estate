<?php
if (!session_id()) {
    session_start();
}
require_once dirname( __FILE__ ) . '/ajax.php';
add_action( 'init', 'titus_init', 1 );


function titus_init()
{
    add_filter( 'show_admin_bar', '__return_false' );

    add_image_size( '600x600', 600, 600, true );
    add_image_size( '600x400', 600, 400, true );

    add_theme_support('post-thumbnails');

    register_taxonomy(
        'actions',
        array(
            'post',
        ),
        array(
            'label' => 'Actions',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'features',
        array(
            'post',
        ),
        array(
            'label' => 'Features',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'city',
        array(
            'post',
        ),
        array(
            'label' => 'City',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'area',
        array(
            'post',
        ),
        array(
            'label' => 'Area',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'type',
        array(
            'post',
        ),
        array(
            'label' => 'Types',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'country',
        array(
            'post',
        ),
        array(
            'label' => 'Country',
            'hierarchical' => false,
        )
    );

    register_taxonomy(
        'state',
        array(
            'post',
        ),
        array(
            'label' => 'State',
            'hierarchical' => false,
        )
    );

    add_role(
        'Agents',
        __( 'Agents' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => true,
            'delete_posts' => false, // Use false to explicitly deny
        )
    );

    if( function_exists('acf_add_options_sub_page') ) {
        acf_add_options_sub_page(array(
            'title' 	=> 'Featured posts',
            'menu' 	=> 'Featured posts',
            'parent' 	=> 'edit.php',
            'slug' 			=> 'featured_posts',
            'capability' => 'edit_posts'
        ));
    }

    if( function_exists('acf_add_options_sub_page') ) {
        acf_add_options_sub_page(array(
            'title' 	=> 'Settings',
            'menu' 	=> 'Settings',
            'parent' 	=> 'titus',
            'slug' 			=> 'titus_settings',
            'capability' => 'edit_posts'
        ));
    }

}

