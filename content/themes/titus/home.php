<?php
/**
 * Created by Karlen Avetisyan.
 * User: karlen
 * Date: 6/18/16
 * Time: 10:55 PM
 */
get_header();
load_module('Slider');
load_module('Search');
load_module('Latest');
load_module('Follow');
load_module('Banner', array());
load_module('Comments');
load_module('Testimonials');
load_module('Banner', array());
get_footer();
