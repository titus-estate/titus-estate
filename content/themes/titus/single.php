<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.WordPress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Titus
 */
global  $post;
get_header();
load_module('Slider', array(
    "data" => get_post_slider_images($post->ID)
));
?>
    <div class="container article-container-main">
        <div class="row">
            <div class="col l12 s12 m12 left_article_content">
                <?php
                load_module('ArticleContent');
                ?>
            </div>
        </div>
    </div>
    <div class="container">
        <?php
            load_module("Similar");
            load_module("ContactUsForm");
        ?>
    </div>
<?php
get_footer();
